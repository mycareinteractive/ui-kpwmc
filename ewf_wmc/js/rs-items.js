var RSItems = View.extend({

    id: 'rs-items',

    template: 'rs-items.html',

    css: 'rs-roomservice.css',


    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
        var $curr = this.$("a.active");
        if (key == 'MENU' || key == 'HOME') {
            this.destroy();
            return true;
        }
		if(key == 'GUIDE') {						
            return false;
        }

        else if (key == 'UP' || key == 'DOWN' || key == 'LEFT' || key == 'RIGHT') { // navigation keys
            SpatialNavigation.move(key.toLowerCase(), this.$('a.active'));
									
			var $focusedItem = this.$('.active');
			if ($focusedItem.hasClass('item-button')) {
				var keyname = $focusedItem.attr('keyname');
				var image = this.imageData[keyname];

				if (typeof image == 'undefined') {
					this.$('.item-image-bk img.item-image').attr('src', './images/dine/dining_image-not-available.jpg');
				} else {
					this.$('.item-image-bk img.item-image').attr('src', './images/dine/' + image);
				}
			}

            return true;
        }
        else if (key == 'ENTER') {  // default link click
            this.key = 'ENTER';
            return this.click($curr);
        }
        return false;
    },

    click: function ($jqobj) {
		
		var linkid = $jqobj.attr('id');
		var alreadyselected = $jqobj.attr('alreadyselected');
		var currLabel = this.className;
		var data = this.data;
		var path = this.pagePath + '/' + linkid;
		
        if (linkid=='finished') { // back button            
        	this.destroy();
            return true;
        }
				
        else if (linkid=='help') { // back button            
        	var popuptext = '<p>Need Help?<br/><br/>Please call your Dietary Team at<br>Ext. 24-3143 for assistance.</p>'
			var buttons = '<a class="footer-button-popup active" id="no" href="">No, Return to Last Screen</a><a class="footer-button-popup" id="yes" href="">Yes, Cancel this Order</a>'
			var buttons = '<a class="footer-button-popup active" id="close" href="">Close</a>'            
            page = new RSInformation({className:linkid, breadcrumb:currLabel, data: data, pagePath: path, popuptext : popuptext, buttons : buttons});			
            page.render();			            
            return true;
        }
				
		else if ($jqobj.hasClass('item-button')) { // course button						
			this._selectItem($jqobj);
			return true;
		}

		
        // handle clicks to somewhere else
        return false;
    },

    renderData: function () {
		this.$('#mask').hide();
		var context = this;
		this._getImageData();
		this.listItems();
		
        // Initialize spatial navigation
        this.$('a').focus(function () {
            context.$('a').removeClass('active');
            $(this).addClass('active');
        });
					
        SpatialNavigation.init();

		SpatialNavigation.add(this.viewId, {
			selector: '#' + context.id + ' a',
		});        // Focus the first navigable element.

        // Focus the first navigable element.
        SpatialNavigation.focus(this.viewId);
    },

    uninit: function () {
        SpatialNavigation.remove(this.viewId);
    },
	
	shown: function () {
		var $focusedItem = this.$('.active');
		if ($focusedItem.hasClass('item-button')) {
			var keyname = $focusedItem.attr('keyname');
			var image = this.imageData[keyname];

			if (typeof image == 'undefined') {
				this.$('.item-image-bk img.item-image').attr('src', './images/dine/dining_image-not-available.jpg');
			} else {
				this.$('.item-image-bk img.item-image').attr('src', './images/dine/' + image);
			}
		}
		return true;

	},
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	listItems : function ()	{
		var context = this;
		var data = this.data;
		
		var context = this;
        context.$('.item-buttons').empty();
        var data = this.data;

        var meals = Array();
        var itemstring = '';
        var mealname = '';
        var selectedcount = 0;
        
       
        var coursename = this.coursename;

		this.$('.page-title').html(coursename)

        var cnt = 0;
		var active = ' active';
		var qty = "";
        $(data).find("Item").each(function () {

            var item = $(this).attr("TrayTicketLongName");
            var id = $(this).attr("Id");
            var keyname = $(this).attr("Keyname");
            
			
            subdata = $(this);
			var html = '<div class="qty">'+qty+'</div><div class="itemname">'+item+'</div>'
            var $entry = $('<a href="#" class="item-button ellipsis' + active + '"></a>').attr('data-index', id).attr('title', item)
                .attr('keyname', keyname).html(html);
		
            $entry.appendTo(context.$('.item-buttons'));
			active = '';
            var items = window.meals;
            var i = 0;

            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var itemid = item["id"];
                if (itemid == id) {
                    $entry.addClass('selected');
                    selectedcount = selectedcount + 1;
                }


            }
            cnt = cnt + 1;
        });

        this.selectedcount = selectedcount;
        // pad some extra lines to fill the last page
        var padNum = this.pageSize - (cnt % this.pageSize);
        if (padNum == 8) padNum = 0;
        for (var i = 0; i < padNum - 1; i++) {
            $('<div class="padding"></div>').appendTo(context.$('.item-buttons'));
        }
        msg(cnt);

    },

      
	_getImageData: function () {
        var url = './Food_Images.xml';
        var imageData = getXMLFile(url);
        var parsedData = eval('(' + imageData + ')');
        this.imageData = parsedData.images;
        return true;
    },

	
	_selectItem: function ($jqobj) {
		
		var ewf = ewfObject();
		var dataobj = {};      
		var currLabel = this.data.label
		var linkid = $jqobj.attr('id');		
		var path = this.pagePath + '/' + linkid;
		
		
	
		page = new RSQtyChange({className:linkid, breadcrumb:currLabel, data: this.data, mealsXML: this.mealsXML, pagePath: path});
		page.render();			            
		return true;
	},

});



