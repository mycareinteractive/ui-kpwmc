var MyNotes = View.extend({

    id: 'mynotes',

    template: 'mynotes.html',

    css: 'mynotes.css',

    mrn: '',
    notesId: '',
    notesValue: '',
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key, e)	{
        var specialKeys = [
            'ENTER', 'HOME', 'END', 'LEFT', 'RIGHT', 'UP', 'DOWN', 'PGUP', 'PGDN',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'CHUP', 'CHDN' ];

        if(specialKeys.indexOf(key)>=0) {
            e.stopBubble = true;
            return false;
        }
        return false;
    },

    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');

        if($jqobj.hasClass('back')) { // back button
            this.destroy();
            return true;
        }

        if(linkid == 'save') { // save it
            if(!this._saveMyNotes()) { // fail to save
                var page = new Dialog({message:'Sorry we are having problem saving your notes.  Please try again later.'});
                page.render();
                return true;
            }
            this.destroy();
            return true;
        }

        return false;
    },

    renderData: function() {
        this._getMRN();
        this._loadMyNotes();
    },

    shown: function () {
        this._focusMyNotes();
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/

    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _getMRN: function() {
        var ewf = ewfObject();
        var mrn, url, xml, dataobj;

        // Get MRN
        url = ewf.getmrn;
        dataobj = {};
        dataobj['room'] = window.settings.room;
        dataobj['bed'] = window.settings.bed;
        dataobj['name'] = window.settings.userFullName;

        xml  = getXdXML(url, dataobj);
        $(xml).find("patientinfo").each(function() {
            mrn = ($(this).find("mrn").text());
        });

        this.mrn = mrn;
    },

    _loadMyNotes: function() {
        var context = this;
        var ewf = ewfObject();
        var url, xml, dataobj;

        // Get support person
        url = ewf.getclinical + "?type=mynotes&mrn=" + this.mrn + "&numrec=1&sortorder=asc"
        dataobj = '';
        xml  = getXdXML(url, dataobj);

        $(xml).find("item").each(function() {
            context.notesId = $(this).find("id").text();
            context.notesValue = unescape($(this).find("value").text());
        });

        if(!context.notesValue)
            return;

        this.$('#notes').val(context.notesValue);
    },

    _saveMyNotes: function() {

        if(this.notesValue && !this.notesId) {
            // server returned notes but not ID, don't save it
            return false;
        }

        var value = this.$('#notes').val();

        var xmlValue = '';
        xmlValue += '<?xml version="1.0" encoding="ISO-8859-1" ?>';
        xmlValue += '<clinicaldata>';
        xmlValue += '<id>' + (this.notesId||'') + '</id>';
        xmlValue += '<ptname>' + window.settings.userFullName + '</ptname>';
        xmlValue += '<mrn>' + this.mrn + '</mrn>';
        xmlValue += '<room>' + window.settings.room + '</room>';
        xmlValue += '<bed>' + window.settings.bed + '</bed>';
        xmlValue += '<type>mynotes</type>';
        xmlValue += '<value>'+ escape(value) + '</value>';
        xmlValue += '</clinicaldata>';

        var ewf = ewfObject();
        var url = ewf.addclinical;
        var ret = postXdXML(url, {patronMenu:xmlValue});
        return ret?true:false;
    },

    _focusMyNotes: function() {
        this.$('#notes').focus();
    }
});