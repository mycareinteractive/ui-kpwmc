var DineDeliveryTimes = View.extend({

    id: 'dinedeliverytimes',

    template: 'dinedeliverytimes.html',

    css: 'dine.css',

    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
        var $curr = this.$("a.active");
        if (key == 'MENU' || key == 'HOME') {
            this.destroy();
            return true;
        }
        else if (key == 'ENTER' && $curr.hasClass('back-button')) {  // default link click
            this.destroy();
            return true;
        }
        else if (key == 'UP' || key == 'DOWN' || key == 'LEFT' || key == 'RIGHT') { // navigation keys
            SpatialNavigation.move(key.toLowerCase(), this.$('a.active'));
            return true;
        }
        else if (key == 'ENTER') {  // default link click
            this.key = 'ENTER';
            return this.click($curr);
        }
        return false;
    },

    click: function ($jqobj) {

        if ($jqobj.hasClass('back-button')) { // back button
            this.destroy();
            return true;
        }

        // handle clicks to somewhere else
        return false;
    },

    renderData: function () {
        var context = this;

        // Initialize spatial navigation
        this.$('a').focus(function () {
            context.$('a').removeClass('active');
            $(this).addClass('active');
        });

        SpatialNavigation.init();

		SpatialNavigation.add(this.viewId, {
			selector: '#' + context.id + ' a',
		});        // Focus the first navigable element.
			
        SpatialNavigation.focus(this.viewId);
    },

    uninit: function () {
        SpatialNavigation.remove(this.viewId);
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/

});



