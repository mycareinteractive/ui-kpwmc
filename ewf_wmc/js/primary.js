var Primary = View.extend({

    id: 'primary',

    template: 'primary.html',

    css: null, // we preload css for primary to eliminate the paint effect

    className: 'home',

    pagePath: '/home',

    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key, e) {

        // home page will ignore these keys so it doesn't close itself
        var ignoredKeys = ['MENU', 'HOME', 'EXIT', 'BACK', 'POWR', 'CLOSE', 'CLOSEALL'];
        if(ignoredKeys.indexOf(key) >= 0) {
            return true;
        }

        if(key == 'CHUP' || key == 'CHDN') {
            this.$('a[data-type="grid"]').first().click();
            return true;
        }
		
		if(key == 'GUIDE') {
			keypressed('CLOSEALL');
            keypressed(216);    //force going back to main menu
			this.openGuide("watchtv", parent, '', 'watchtv');
            return true;
        }
			
		var pagePath = this.pagePath + '/';
		if (key == '0' || key == '1' || key == '2' || key == '3' || key == '4' || key == '5' || key == '6' || key == '7' || key == '8' || key == '9') {
			switch (key) {
				case '0':    // Languages
					this.openLanguages("languages", parent, '', 'languages');
					break;
				case '1':    // WatchTV
					this.openGuide("watchtv", parent, '', 'watchtv');
					break;
				case '2': 	//order food
					var linkid = 'orderfood';
					var itemData = this._getMenuItemDatabyID('orderfood','thrive');
					var pagePath = '/home/thrive/orderfood';
					var parent = 'Thrive';
					this.openSecondary(linkid, parent, itemData, pagePath);                
					break;
				case '3': 	//My Education
					var linkid = 'orderfood';
					var itemData = this._getMenuItemDatabyID('myeducation','learn');
					var pagePath = '/home/learn/myeducation';
					var parent = 'Learn';
					this.openMyPrograms(linkid, parent, itemData, pagePath);
					break;
				case '4': 	//Movies
					var linkid = 'movies';
					var itemData = this._getMenuItemDatabyID('movies','fun');
					var pagePath = '/home/fun/movies';
					var parent = 'Fun';
					this.openMovies(linkid, parent, itemData, pagePath);
					break;
				case '5': 	//Feedback
					var linkid = 'feedback';
					var itemData = this._getMenuItemDatabyID('feedback','visit');
					var pagePath = '/home/visit/feedback';
					var parent = 'Visit';
					this.openFeedback(linkid, parent, itemData, pagePath);					
					break;
				case '6': 	//Internet
					var linkid = 'internet';
					var itemData = this._getMenuItemDatabyID('internet','fun');
					var pagePath = '/home/fun/internet';
					var parent = 'Fun';
					this.openSecondary(linkid, parent, itemData, pagePath);                
					break;
				case '7': 	//My Support
					var linkid = 'aboutmystay';
					var itemData = this._getMenuItemDatabyID('aboutmystay','visit');
					var pagePath = '/home/visit/aboutmystay';
					var parent = 'Visit';
					var quickButton = 'mysupportperson';
					this.openSecondary(linkid, parent, itemData, pagePath, quickButton);                
					break;
				case '8': 	//Mail
					var linkid = 'aboutmystay';
					var itemData = this._getMenuItemDatabyID('aboutmystay','visit');
					var pagePath = '/home/visit/aboutmystay';
					var parent = 'Visit';
					var quickButton = 'eleafmail';
					this.openSecondary(linkid, parent, itemData, pagePath, quickButton);                
					break;
					

			}
			return true;
		}

        // fallback to super because it handles drop-down menu
        return this._super(key, e);
    },

    click: function ($jqobj) {
		msg($jqobj);
		msg('in click');
        var ret = false;
        var context = this;
        var patientDATA = loadJSON('patient');
        var room = patientDATA.roomNumber;

        var linkid = $jqobj.attr('id');
        // stupid legacy code
        $("#K_submenu").removeClass().addClass(linkid);
        $("#K_submenu").text(linkid);		
		
		if($jqobj.hasClass('menu-group-button')) {
                this._focusMajor($jqobj);
                return true;
        }

		if ($jqobj.parent().parent().attr('class') == 'minor') { // menu items
            var parent = $jqobj.parent().attr('title');
            var pagePath = this.pagePath + '/' + $jqobj.parent().attr('name') + '/' + linkid;
            var type = $jqobj.attr('data-type');
            var itemData = this._getMenuItemData($jqobj);
			var processed = false;
            msg(itemData);

			msg('primary.js - click ' + linkid);

            switch (linkid) {
                case 'movies':    // movie
                    this.openMovies(linkid, parent, itemData, pagePath);
                    return true;
                case 'myeducation':
                    this.openMyPrograms(linkid, parent, itemData, pagePath);
                    return true;
                case 'feedback':
                    this.openFeedback(linkid, parent, itemData, pagePath);
                    return true;                   
                case 'allprograms':
                    this.openAllPrograms(linkid, parent, itemData, pagePath);
                    return true;
				case 'languages':
                    this.openLanguages(linkid, parent, itemData, pagePath);
                    return true;
				case 'welcomevideo':
                case 'scenictv':
                    this.openVideoPlayer2(linkid, parent, itemData, pagePath);
                    return true;
                case 'mymenu':
                    this.openDine(linkid, parent, itemData, pagePath);
                    return true;
            }
			
			switch (type) {
                case 'link':    // website
                    this.openWebpage(linkid, parent, itemData, pagePath);
                    break;
                case 'menu':    // next level menu
                    this.openSecondary(linkid, parent, itemData, pagePath);
                    break;
                case 'grid':    // guide
                    this.openGuide(linkid, parent, itemData, pagePath);
                    break;
            }

            ret = true;
        }		
        else if ($jqobj.parent().parent().attr('id') == 'menubar' || $jqobj.parent().parent().attr('id') == 'menu') { // menu bar drop down click
            ret = true;
        }

        return ret;
    },

    renderData: function () {
        this.label = "Home";
        this._buildPage();
        this._buildMenu();


        this.$el.show();

        var context = this;

        /*
		// start slideshow
        this.$('#slideshow img:gt(0)').hide();
        $.doTimeout('primary slideshow', 5000, function () {
            context.$('#slideshow :first-child').fadeOut(1200)
                .next('img').fadeIn(1200)
                .end().appendTo(context.$('#slideshow'));
            return true;
        });
        */
        this.$('#revision').html('UpCare ' + window.package.version + '<br>' + window.settings.version.replace('PROCENTRIC', 'LG') + ' ' + window.settings.platformVersion);

        this._loadMySupport();
    },

    uninit: function () {
        $.doTimeout('primary clock'); // stop clock
        $.doTimeout('primary slideshow'); // stop slideshow
    },

    refresh: function () {
        this._buildPage();
        this._buildMenu();
        $.doTimeout('primary clock', true);
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    open: function (tagName) {
        var $obj = this.$('#menus a#' + tagName);
        if ($obj.length == 1) {
            this.click($obj);
        }
    },

    openSecondary: function (linkid, breadcrumb, data, pagePath, quickButton) {
        var context = this;
        var page = new Secondary({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath, quickButton: quickButton, 
            oncreate: function () {                
            },
            ondestroy: function () {                
            }
        });
        page.render();
    },

   
   
    openGuide: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new TVGuide({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {
            },
            ondestroy: function () {
            }
        });
        page.render();
    },

    openMovies: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new Movies({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {                
            },
            ondestroy: function () {
            }
        });
        page.render();
    },

    openMyPrograms: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new MyPrograms({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {
            },
            ondestroy: function () {
            }
        });
        page.render();
    },

    openAllPrograms: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new AllPrograms({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {
            },
            ondestroy: function () {
            }
        });
        page.render();
    },

    openFeedback: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new Feedback({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {
            },
            ondestroy: function () {
            }
        });
        page.render();
    },
    
    openComingSoon: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new Dialog({
            className: linkid, breadcrumb: breadcrumb, data: data, message: 'Coming Soon...', pagePath: pagePath,
            oncreate: function () {
            },
            ondestroy: function () {
            }
        });
        page.render();
    },

    openVideoPlayer2: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
        var type, url, display;
        if (data && data.attributes && data.attributes.attribute && data.attributes.attribute.text) {
            var params = data.attributes.attribute.text.split(',');
            type = params[0];
            url = params[1];
            display = params[2] || '';

            var page = new VideoPlayer2({
                className: linkid,
                breadcrumb: breadcrumb,
                data: data,
                type: type,
                url: url,
                display: display,
                pagePath: pagePath,
                oncreate: function () {
                },
                ondestroy: function () {
                }
            });
            page.render();
        }
    },

    openLanguages: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
		var oldLang = window.settings.language;
        var page = new Languages({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {
            },
            ondestroy: function () {
                var newLang = window.settings.language;
                // when closed, determine if we need to translate primary page.
                if (newLang != oldLang) {
                    context.translate(newLang);
                    context.refresh();
                }
            }
        });
        page.render();
    },

    openDine: function (linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new DineCourse({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {
            },
            ondestroy: function () {
            }
        });
        page.render();
    },
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

    _buildPage: function () {

        var patientDATA = loadJSON('patient');
        var patientstatus = patientDATA.status;
        var patientname = patientDATA.userFullName;
        var firstnames = ""; // <- forgot to initialize to an empty string
        /*if (typeof patientname != 'undefined') {
            var splitName = patientname.split(" ");
            var surname = splitName[splitName.length - 1]; //The last one
            for (var i = 0; i < splitName.length - 1; i++) {
                firstnames += splitName[i] + " ";
            }
        }*/
        var room = patientDATA.roomNumber.toUpperCase();
        var phone = patientDATA.phoneNumber;
        var roomInfo = "Room " + room;
        roomInfo += (phone) ?  ", Phone " + phone : "";

		msg("Room: " + room);
		msg("Phone: " + phone);

        var $p  = window.parent.$;

        $p("#ewf-footer").contents().find('#footer #patientname p').text('"' +patientname + '"');
        $p("#ewf-footer").contents().find('#footer #patientroom p').text(roomInfo);

        $p("#ewf-footer").contents().find('#footer #patient p.phone').text('Room Phone: ' +patientDATA.phoneNumber);
		$p("#ewf-footer").contents().find('#footer #revision').html('UpCare ' + window.package.version + '<br/>' + window.settings.version.replace('PROCENTRIC', 'LG') + ' ' + window.settings.platformVersion);
    },

    _buildMenu: function() {
        this.$('#navigation #menubar').empty();
        this.$('#navigation #menus').empty();
        var ewf = ewfObject();

        getMenuXML();
        if($("#XMLCALL_error").text().indexOf("ERROR") >= 0){
            msg('### Unable to get MENU data!!');
            this.$('#navigation #menubar').text('Oops! Unable to get MENU data!!');
            return;
        }
        

        this.data = getJsonMenuFromXML().response.menuBar;
        var menuObj = this.data.menu;
        

        var $menu = $('<div id="menu"></div>');
        var $menutabs = $('<div id="major lazy"></div>');
        var $menus = $('<div id="minor"></div>');
        var cnt = 0;

        var context = this;
		var style = '';
		var hrefclass = '';		
		
        $.each(menuObj, function(i,row){
			cnt = cnt + 1;
            //set the menu that will be active at startup this is special for Sunnyside
			if (cnt==ewf.activemajormenu) {
				style = "display:block";
				hrefclass = "menu-group-button selected";				
			} else {
				style = "";
				hrefclass = "menu-group-button";
			}			
			
            $('<a href="#"></a>').attr('class', hrefclass).attr('id', 'menu'+cnt).attr('name', row['tag']).text(row['label']).appendTo($menu);
            $('<div class="major lazy"></div>').attr('id', row['tag']).text(row['label']).appendTo($menutabs);
            var $submenu = context._buildSubMenu(row, row["tag"],cnt);
				
            var o = $('<div class="menu-group"></div>').attr('id','menu'+cnt+'-group').attr('name',row['tag']).attr('title', row['label']).attr('audio',row['image']||'').attr('data-index',i).attr('style',style).append($submenu.html()).appendTo($menus);																			
        });		

        this.$('#content #menu .major.lazy').html($menu.html());
        this.$('#content #menu .minor').html($menus.html());	
    },
    

    _buildSubMenu: function (menu, tagName, menucnt)   {
		var cnt = 1;
        var $submenu = $('<div"></div>');
        var items = menu[tagName];
		var hrefclass = '';
		var ewf = ewfObject();
			
        if(items.label) { // single object
            var item = items;
            $('<a href="#"></a>').attr('name', item.tag).attr('class', 'active').attr('id', item.tag).attr('audio', item.image||'').attr('title', item.label).attr('data-type', item.type).attr('data-index',-1).text(item.label).appendTo($submenu);
        } 

        else { // array			
            $.each(items, function(i,item){          				
				if(menucnt == ewf.activemajormenu && cnt == ewf.activeminormenu) 
					hrefclass = 'active ellipsis expandable'
				else 
					hrefclass = 'ellipsis';		
                $('<a href="#"></a>').attr('name', item.tag).attr('class', hrefclass).attr('id', item.tag).attr('audio', item.image||'').attr('title', item.label).attr('data-type', item.type).attr('data-index',i).text(item.label).appendTo($submenu);
				cnt = cnt + 1;
            });
        }
        

        return $submenu;
    },
    
    _getMenuItemData: function ($obj) {
        var itemData = null;
        var itemTag = $obj.attr('id');
        var parentTag = $obj.parent().attr('name');
		
		msg(itemTag + ' ' + parentTag);
        $.each(this.data.menu, function(i,menu){		
            if(menu.tag == parentTag) {
                var items = menu[menu.tag];				
                if($.isArray(items)){ // an array of items
                    $.each(items, function(i,item){
                        if(item.tag == itemTag) {						
                            itemData = item;
                            return false;   // break
                        }
                    });
                }
                else { // only one item
                    if(items.tag == itemTag)
                        itemData = items;
                }
                return false; // break
            }
        });

        return itemData;
    }, 
	
	 _getMenuItemDatabyID: function (id,parent) {
        var itemData = null;
        var itemTag = id;
        var parentTag = parent;
        $.each(this.data.menu, function(i,menu){		
            if(menu.tag == parentTag) {
                var items = menu[menu.tag];				
                if($.isArray(items)){ // an array of items
                    $.each(items, function(i,item){
                        if(item.tag == itemTag) {						
                            itemData = item;
                            return false;   // break
                        }
                    });
                }
                else { // only one item
                    if(items.tag == itemTag)
                        itemData = items;
                }
                return false; // break
            }
        });

        return itemData;
    },

    _loadMySupport: function() {
        var context = this;
        var ewf = ewfObject();
        var url, xml, dataobj;

        // Get support person
        url = ewf.getclinical + "?type=mysupport&mrn=" + this.mrn + "&numrec=1&sortorder=asc"
        dataobj = '';
        xml  = getXdXML(url, dataobj);

        msg('%%%%%%% SUPPORT XML %%%%%%%');
        msg(xml);
        msg('%%%%%%% %%%%%%% %%% %%%%%%%');

        $(xml).find("item").each(function() {
            context.supportId = $(this).find("id").text();
            context.supportValue = unescape($(this).find("value").text());
        });

        if(!context.supportValue)
            return;

        var vals = context.supportValue.split('\t');
        var names = vals[0]? vals[0].split(' '): ['',''];
        var support = vals[0];
        var phone = vals[1];

        this.$('#firstname').val(names[0]||'');
        this.$('#lastname').val(names[1]||'');
        this.$('#phonenumber').val(phone||'');

        var $p  = window.parent.$;
        if(support) {
            support += (phone) ? "&nbsp;" + phone : "";

            $p("#ewf-footer").contents().find('#supportperson > p').append("&nbsp;" + support);
            $p("#ewf-footer").contents().find('#supportperson').show();
        }
    },

});