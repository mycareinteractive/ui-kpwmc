var RSCategories = View.extend({

    id: 'rs-categories',

    template: 'rs-categories.html',

    css: 'rs-roomservice.css',


    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
        var $curr = this.$("a.active");
        if (key == 'MENU' || key == 'HOME') {
            this.destroy();
            return true;
        }
		if(key == 'GUIDE') {						
            return false;
        }

        else if (key == 'UP' || key == 'DOWN' || key == 'LEFT' || key == 'RIGHT') { // navigation keys
            SpatialNavigation.move(key.toLowerCase(), this.$('a.active'));
            return true;
        }
        else if (key == 'ENTER') {  // default link click
            this.key = 'ENTER';
            return this.click($curr);
        }
        return false;
    },

    click: function ($jqobj) {
		
		var linkid = $jqobj.attr('id');
		var alreadyselected = $jqobj.attr('alreadyselected');
		var currLabel = this.className;
		var data = this.data;
		var path = this.pagePath + '/' + linkid;
		var coursemax = $jqobj.attr('max');
        var coursename = $jqobj.attr('title');
		
        if (linkid=='back') { // back button  
			clearDineData();		
        	this.destroy();
            return true;
        }
		
		else if (linkid=='cancel') { // cancel button            
        	var title = 'Warning';			
			var popuptext = '<p>Are you sure you would like to<br/>exit Room Service Ordering<br/>and Cancel this order?<br/><br/><span class="text-color-2-lg">Any orders you have NOT placed<br/>will be cancelled.</span></p>'
			var buttons = '<a class="footer-button-popup active" id="no" href="">No, Return to Last Screen</a><a class="footer-button-popup" id="yes" href="">Yes, Cancel this Order</a>'            
            page = new RSInformation({className:linkid, breadcrumb:currLabel, data: data, pagePath: path, pagetitle2 : title, popuptext : popuptext, buttons : buttons});			
            page.render();			            
            return true;
        }

			
        else if (linkid=='help') { // back button            
        	var popuptext = '<p>Need Help?<br/><br/>Please call your Dietary Team at<br>Ext. 24-3143 for assistance.</p>'
			var buttons = '<a class="footer-button-popup active" id="no" href="">No, Return to Last Screen</a><a class="footer-button-popup" id="yes" href="">Yes, Cancel this Order</a>'
			var buttons = '<a class="footer-button-popup active" id="close" href="">Close</a>'            
            page = new RSInformation({className:linkid, breadcrumb:currLabel, data: data, pagePath: path, popuptext : popuptext, buttons : buttons});			
            page.render();			            
            return true;
        }
		
	
		else if (linkid=='view') { // back button            
			this.$('#mask').show();

			var context = this;
			$.doTimeout('dinemeals clock', 200, function () {
				context._categories($jqobj);
				return false;
			});

			return true;
		}

		else if ($jqobj.hasClass('course-button')) { // course button
			this.$('#mask').show();
			this._selectCourse($jqobj, coursemax, coursename);
			return true;
		}

		
        // handle clicks to somewhere else
        return false;
    },

    renderData: function () {
		this.$('#mask').hide();
		var context = this;

		this.listCourses();
		
        // Initialize spatial navigation
        this.$('a').focus(function () {
            context.$('a').removeClass('active');
            $(this).addClass('active');
        });
					
        SpatialNavigation.init();

        SpatialNavigation.add(this.viewId, {
			selector: '#' + context.id + ' a',
		});

        // Focus the first navigable element.
        SpatialNavigation.focus(this.viewId);
    },

    uninit: function () {
        SpatialNavigation.remove(this.viewId);
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	listCourses : function ()	{
		var context = this;
		var data = this.data;
		
        var meals = Array();
        var coursestring = '';
        var mealname = ''; 
		var active = ' active ';
		var selected = '';
		var courseselected = window.coursesselected;	
        $(data).find("Person").each(function () {
            $(data).find("Meals").each(function () {
                $(data).find("Meal").each(function () {
                    mealname = $(this).attr("Name");
                    $(data).find("ServiceCourse").each(function () {

                        var course = $(this).attr("Name");
						var coursename = course;
                        var max = $(this).attr("MaxSelection");
                        var id = $(this).attr("Id");
						if (typeof courseselected[course] == 'undefined') 					
							selected = '';
						else 
							selected = ' &#10004;'
							
						
						if(course == 'Beverages' || course == 'Condiments')
							coursename = '<span class="text-color-2">' + coursename + '</span>'
																		
                        coursestring = coursestring + '<a href="#" title = "' + course + '"class = "course-button ' + course + active + '" max="' + max + '" id="' + id + '">' + coursename + selected + '</a>';
						active = '';
						
                    });
                });
            });
        });		
			
		if (mealname == 'PM SNACK')
			mealname = 'AFTERNOON SNACK'
		if (mealname == 'AM SNACK')
			mealname = 'MORNING SNACK'
		
		mealname = titleCase(mealname);

		this.$('.course-buttons').html(coursestring);		
		this.$('#heading1').html('<p class="text-large">' + mealname + ':</p>')
		

	},
	
	_selectCourse: function ($jqobj, coursemax, coursename) {
		
		var ewf = ewfObject();
		var dataobj = {};        

		var context = this;
		var data = this.data;
		
		var currLabel = this.data.label
		var linkid = $jqobj.attr('id');
		var path = this.pagePath + '/' + linkid;


		var coursestring = '';
		var mealname = '';
		window.settings.coursemax = coursemax;
		$(data).find("Person").each(function () {
			$(data).find("Meals").each(function () {
				$(data).find("Meal").each(function () {
					$(data).find("ServiceCourse").each(function () {
						var id = $(this).attr("Id");
						var subdata = $(this);
						if (id == linkid) {
							context.$('#mask').hide();

							page = new RSItems({className:linkid, breadcrumb:currLabel, data: subdata, mealsXML: this.mealsXML, pagePath: path, coursemax: coursemax, coursename: coursename});
							page.render();			            		
						}

					});
				});
			});
		});
		
		return true;
	},
});



