var Intro = View.extend({
    
    id: 'intro',
    
    template: 'intro.html',
    
    css: 'intro.css',
    
    className: 'languages',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
		this.key = key;
		var $curr = $('#intro a.active');
        if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME' || key == 'GUIDE') {
            //intro page never closes
            return true;
        }
        else if(key == 'POWR') {
            return true;
        }
        
        var navkeys = ['UP','DOWN'];
        if(navkeys.indexOf(key)!=-1) {
            this.changeFocus(key);
			var $curr = $('#intro a.active');
			return this.click($curr);
        }
        
        return false;
    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        window.settings = window.settings || {};
        window.settings.language = linkid;
        if(this.key=='ENTER') {
			if(linkid == 'en' || linkid == 'es' || linkid == 'vi') {
				setLanguage(linkid); // send language back to server
				var intro2 = new Intro2({});
				intro2.render();
				return true;
			}
		} else  {
			
            this.$('#instruction.en').hide();
			this.$('#instruction.es').hide();
			this.$('#instruction.vi').hide();
            this.$('#instruction.' + linkid).show();

		}
        
        return false;
    },
    
    renderData: function() {
        var context = this;			
			this.$('#instruction.es').hide();
			this.$('#instruction.vi').hide();

		
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
});

