var Intro2 = View.extend({

    id: 'intro2',

    template: 'intro2.html',

    css: 'intro2.css',

    className: 'agreement',

    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
		this.key = key;
        var $curr = $('#intro2 a.active');
        var $next = $curr.next();
        if ($next.length <= 0)
            $next = $curr.prev();

        var ignoredKeys = ['MENU', 'HOME', 'EXIT', 'BACK', 'POWR', 'CLOSE', 'CLOSEALL', 'GUIDE'];
        if(ignoredKeys.indexOf(key) >= 0) {
            return true;
        }

        else if (key == 'ENTER') {  // default link click
            return this.click($curr);
        }
        else if (key == 'UP' || key == 'DOWN') {
            this.blur($curr);
            this.focus($next);
			return true;
        }

        return false;
    },

    click: function ($jqobj) {
        var linkid = $jqobj.attr('id');
		if (linkid == 'accept') {
            // tracking
            nova.tracker.event('language', 'set', window.settings.language);
            nova.tracker.event('terms', 'accept');

            // close both intro page and intro2 page
            window.pages.closePage('intro');
            this.destroy();

            // build primary page
            var page = new Primary({});
            page.render();
            return true;
        }
        else if (linkid == 'decline') {

            if(window.settings.language != '') {
                // For every patient we only track decline once.
                // If the patient already declined then the language is empty string instead of undefined
                nova.tracker.event('terms', 'decline');
            }

            // Reset language
            setLanguage('');

            // Close primary page
            window.pages.closePage('primary');
            this.destroy();

            // Open TV guide
            this.watchTV(linkid);
            return true;
        }

        return false;
    },

    renderData: function () {
        var context = this;
		var lang = window.settings.language;
		if(lang == 'en') {
			this.$('#regtext.es').hide();
			this.$('#regtext2.es').hide();
			this.$('#buttons.es').hide();
			this.$('#regtext.vi').hide();
			this.$('#regtext2.vi').hide();	
			this.$('#buttons.vi').hide();			
		} else if(lang == 'es') {
			this.$('#regtext.en').hide();
			this.$('#regtext2.en').hide();
			this.$('#buttons.en').hide();
			this.$('#regtext.vi').hide();
			this.$('#regtext2.vi').hide();			
			this.$('#buttons.vi').hide();
		} else if(lang == 'vi') {
			this.$('#regtext.en').hide();
			this.$('#regtext2.en').hide();
			this.$('#buttons.en').hide();
			this.$('#regtext.es').hide();
			this.$('#regtext2.es').hide();			
			this.$('#buttons.es').hide();
		}

    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    watchTV: function (className) {

        var linkId = 'tv';
        var pagePath = this.pagePath + '/' + linkId;
        var page = new TVGuide({className: className, pagePath: pagePath});
        page.render();
    }
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

});

