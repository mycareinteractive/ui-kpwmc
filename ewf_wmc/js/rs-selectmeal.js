var RSSelectMeal = View.extend({

    id: 'rs-selectmeal',

    template: 'rs-selectmeal.html',

    css: 'rs-roomservice.css',


    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
        var $curr = this.$("a.active");
        if (key == 'MENU' || key == 'HOME') {
            this.destroy();
            return true;
        }
		if(key == 'GUIDE') {						
			clearDineData();
            return false;
        }

        else if (key == 'UP' || key == 'DOWN' || key == 'LEFT' || key == 'RIGHT') { // navigation keys
            SpatialNavigation.move(key.toLowerCase(), this.$('a.active'));
            return true;
        }
        else if (key == 'ENTER') {  // default link click
            this.key = 'ENTER';
            return this.click($curr);
        }
        return false;
    },

    click: function ($jqobj) {
		
		var linkid = $jqobj.attr('id');
		var alreadyselected = $jqobj.attr('alreadyselected');
		var currLabel = this.className;
		var data = this.data;
		var path = this.pagePath + '/' + linkid;
		
        if (linkid=='exit') { // back button            
        	var title = 'Warning';			
			var popuptext = '<p>Are you sure you would like to<br/>exit Room Service Ordering<br/>and Cancel this order?<br/><br/><span class="text-color-2-lg">Any orders you have NOT placed<br/>will be cancelled.</span></p>'
			var buttons = '<a class="footer-button-popup active" id="no" href="">No, Return to Last Screen</a><a class="footer-button-popup" id="yes" href="">Yes, Cancel this Order</a>'            
            page = new RSInformation({className:linkid, breadcrumb:currLabel, data: data, pagePath: path, pagetitle2 : title, popuptext : popuptext, buttons : buttons});			
            page.render();			            
            return true;
        }
			
        else if (linkid=='help') { // back button            
        	var popuptext = '<p>Need Help?<br/><br/>Please call your Dietary Team at<br>Ext. 24-3143 for assistance.</p>'
			var buttons = '<a class="footer-button-popup active" id="close" href="">Close</a>'            
            page = new RSInformation({className:linkid, breadcrumb:currLabel, data: data, pagePath: path, popuptext : popuptext, buttons : buttons});			
            page.render();			            
            return true;
        }
		
		else if (linkid=='afterhours1') { // after hours meal
			var title = 'After Hours Snacks';			
        	var popuptext = '<p><br/>We have snacks available after<br>the kitchen is closed. <br/><span class="text-color-2-lg">Please press the red call button<br/>and a member of your care team<br>will be by to assist you.</span></p>'
			var buttons = '<a class="footer-button-popup active" id="close" href="">Close</a>'            
            page = new RSInformation({className:linkid, breadcrumb:currLabel, data: data, pagePath: path, pagetitle2 : title, popuptext : popuptext, buttons : buttons});			
            page.render();			            
            return true;
        }

				
		else if (alreadyselected == 'Y') {
			return true;
		}
		
		else if (alreadyselected != 'Y') {
			this.$('#mask').show();

			var context = this;
			$.doTimeout('dinemeals clock', 500, function () {
				context._selectMeal($jqobj);
				return false;
			});

			return true;
		}
		
        // handle clicks to somewhere else
        return false;
    },

    renderData: function () {
		this.$('#mask').hide();
		var context = this;

		this.listMeals();
		
        // Initialize spatial navigation
        this.$('a').focus(function () {
            context.$('a').removeClass('active');
            $(this).addClass('active');
        });
					
        SpatialNavigation.init();

		SpatialNavigation.add(this.viewId, {
			selector: '#' + context.id + ' a',
		});        // Focus the first navigable element.

        // Focus the first navigable element.
        SpatialNavigation.focus(this.viewId);
    },

    uninit: function () {
        SpatialNavigation.remove(this.viewId);
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	listMeals : function ()	{
		var context = this;
		var data = this.mealsXML;
		// start clock
		$.doTimeout('dinemeals clock', 30000, function () {
			var d = new Date();
			var format = 'h:MM tt';
			var locale = window.settings.language;
			context.$('#time').text("Time now: " + d.format(format, false, locale));
			return true;
		});
		$.doTimeout('dinemeals clock', true); // do it now

		var selections = this.$('.meal-butttons');
		var meals = [];
		var mealsstring = '';
		var hintstring = '';
		var excludesnacks = false;
		var afterhoursadded = false;
		var cnt = 0;
		var active = " active";
		$(data).find('Person Meals Meal').each(function () {
												
			var meal = $(this).attr("Name");
			if (meal == 'PM SNACK')
				meal = 'AFTERNOON SNACK'
			if (meal == 'AM SNACK')
				meal = 'MORNING SNACK'
			
			var mealdesc = titleCase(meal);

			var id = $(this).attr("Id");
			var dietorder = $(this).attr("DietOrderId");
			var diet = $(this).attr("Diet");

			var canorder = $(this).attr("CanOrder");
			var latestordertime = $(this).attr("LatestOrderTime");
			var alreadyselected = $(this).attr("AlreadySelected");
			var availableinfo = "Available for Order";
			if (alreadyselected == 'Y')
				availableinfo = '<span class="text-color-2">Ordered - call 24-3463 to change order</span>';


			lastordertime = parseXMLTime(latestordertime);

			//var lastordertime = formatdateTime("yyyy-mm-dd HH:MM:ss",lastordertime);
			//var now = formatdateTime("yyyy-mm-dd HH:mm:ss");
			var now = Date();
			datecompare = compareDates(lastordertime, now);

			if (diet == 'RENAL DIET' || diet == 'LOW PHOSPHORUS' || diet == 'POTASSIUM, 2 GM' || diet == 'SODUIM DIET, 2 GM L/S' || diet == 'CARDIAC DIET' || diet == 'PROTEIN DIET, 40 GM' || diet == 'PROTEIN DIET, 70 GM' || diet == 'PROTEIN DIET, 100 GM' || diet == 'LOW FAT' || diet == 'MINIMAL FAT')
				excludesnacks = true;

			if (meal == 'BREAKFAST') {
				if (typeof meals[meal] == 'undefined') {
					if (canorder == 'Y' && datecompare >= 0) {
						mealsstring = mealsstring + '<a href="#" class="meal-button' + active +'" id="' + id + '" dietorder="' + dietorder + '" alreadyselected="' + alreadyselected + '">' + mealdesc + ' Today</a>';
						hintstring = hintstring + '<p>' + availableinfo + '</p>';
						active = '';
					}
				} else {
					if (canorder == 'Y' && datecompare >= 0) {
						mealsstring = mealsstring + '<a href="#" class = "meal-button afterhours' + active +'" id="afterhours" alreadyselected="N">After Hours</a>';
						hintstring = hintstring + '<p>Available for Order</p>';
						mealsstring = mealsstring + '<a href="#" class = "meal-button" id="' + id + '" dietorder="' + dietorder + '" alreadyselected="' + alreadyselected + '">' + mealdesc + ' Tomorrow</a>';
						hintstring = hintstring + '<p>' + availableinfo + '</p>';
						afterhoursadded = true;
						active = '';
					}
				}
			}
			else if (typeof meals[meal] == 'undefined') {
				if (meal.indexOf('SNACK') >= 1 && excludesnacks == true) {
				} else {

					if (canorder == 'Y' && datecompare >= 0) {
						mealsstring = mealsstring + '<a href="#" class = "meal-button' + active +'" id="' + id + '" dietorder="' + dietorder + '" alreadyselected="' + alreadyselected + '">' + mealdesc + '</a>';
						hintstring = hintstring + '<p>' + availableinfo + '</p>';
						active = '';
					}
				}
			}
			meals[meal] = meal;			
		});
		
		if(afterhoursadded==false) {
			mealsstring = mealsstring + '<a href="#" class = "meal-button afterhours' + active +'" id="afterhours" alreadyselected="N">After Hours</a>';
			hintstring = hintstring + '<p>Available for Order<p>';
		}
			
		this.$('.meal-buttons').html(mealsstring);
		this.$('.meal-text').html(hintstring);

	},
	
	_selectMeal: function ($jqobj) {
		
		var ewf = ewfObject();
		var dataobj = {};
		
		var currLabel = this.data.label
		var linkid = $jqobj.attr('id');		
		var path = this.pagePath + '/' + linkid;
		
		var dietorder = $jqobj.attr('dietorder');
		dataobj = {};
		dataobj['mrn'] = window.settings.mrn;
		dataobj['vendor'] = window.settings.vendor;
		dataobj['meal'] = linkid;
		var url = ewf.dinehost + ":" + ewf.getFood;

		var xml = getXdXML(url, dataobj);

		var resultXML = $(xml).find('GetFoodsResult').text();
		
		var $foodData = $($.parseXML(resultXML));
		window.settings.dietorder = dietorder;
		window.settings.meal = linkid;
		window.settings.deliverytimes = $foodData.find('DeliveryTimes');		
		window.meals = window.meals || new Array();
		window.coursesselected = window.coursesselected || new Array();
		this.$('#mask').hide();

		page = new RSCategories({className:linkid, breadcrumb:currLabel, data: $foodData, mealsXML: this.mealsXML, pagePath: path});
		page.render();			            
		return true;

	},
});



