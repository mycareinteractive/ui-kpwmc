var Languages = View.extend({
    
    id: 'languages',
    
    template: 'languages.html',
    
    css: 'languages.css',
    
    language: null,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {	
		this.key = key;
		var $curr = $('#languages a.active');
		var $next = $curr.next();
		if($next.length<=0) 
			$next = $curr.prev();
			
		if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {            
			this.destroy();
			return true; 			
        }
		else if(key == 'GUIDE') {
			return false;
		}
        else if(key == 'POWR') {
            return true;
        }
		else if (key == 'ENTER') {  // default link click             								
			return this.click($curr);
		}

        var navkeys = ['UP','DOWN'];
        if(navkeys.indexOf(key)!=-1) {
            this.changeFocus(key);
			var $curr = $('#languages a.active');
			return this.click($curr);
            return true;
        }

    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
		if(this.key=='ENTER') {
			if(linkid == 'en' || linkid == 'es' || linkid == 'vi') {          
				var lang = linkid;
						   
				//save it
				window.settings.language = lang;
				// send language back to server
				setLanguage(lang); 
				
				$('#primary').removeClass('en es vi')
				$('#primary').addClass(lang)

				// tracking
				nova.tracker.event('language', 'switch', lang);
				
				// just close, the primary page will detect language change and translate itself
				this.destroy();
				return true;
			}
		} else  {
			
            this.$('#instruction.en').hide();
			this.$('#instruction.es').hide();
			this.$('#instruction.vi').hide();
            this.$('#instruction.' + linkid).show();

		}
			              
        return false;
    },

    renderData: function() {
        var context = this;
		var lang = window.settings.language;
        this.$('#body div').hide();
        this.$('#body #instruction').show();        
		if(lang == 'en') {
			this.$('#buttons #en').addClass('active');			
			this.$('#instruction.es').hide();
			this.$('#instruction.vi').hide();			
		} else if(lang == 'es') {
			this.$('#buttons #es').addClass('active');
			this.$('#instruction.en').hide();
			this.$('#instruction.vi').hide();
		} else if(lang == 'vi') {
			this.$('#buttons #vi').addClass('active');
			this.$('#instruction.en').hide();
			this.$('#instruction.es').hide();
		}

    }
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
});



