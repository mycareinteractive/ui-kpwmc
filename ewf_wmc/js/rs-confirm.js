var RSConfirm = View.extend({

    id: 'rs-confirm',

    template: 'rs-confirm.html',

    css: 'rs-roomservice.css',

    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
        var $curr = this.$("a.active");
        if (key == 'MENU' || key == 'HOME') {
            this.destroy();
            return true;
        }
		
		if(key == 'GUIDE') {			
			this.destroy();
            return false;
        }

        else if (key == 'ENTER' && $curr.hasClass('back-button')) {  // default link click
            this.destroy();
            return true;
        }
        else if (key == 'UP' || key == 'DOWN' || key == 'LEFT' || key == 'RIGHT') { // navigation keys
            SpatialNavigation.move(key.toLowerCase(), this.$('a.active'));
            return true;
        }
        else if (key == 'ENTER') {  // default link click
            this.key = 'ENTER';
            return this.click($curr);
        }
        return false;
    },

    click: function ($jqobj) {
		var currLabel = this.className;
		var data = this.data;
		var linkid = $jqobj.attr('id');	
				
        if (linkid == 'back') { // back button
            this.destroy();
            return true;
        }

        if (linkid == 'notme') { // "This is not me" button
            var path = this.pagePath + '/' + linkid;			
			var popuptext = '<p>If the Patient listed is not you,<br>press the RED call button on your Pillow Speaker. A member of your care team<br>will be by to assist you. <br><br><span class="text-color-2-lg">For ordering assistance, please call Ext. 24-3463.</span></p>'
			var buttons = '<a class="footer-button-popup active" id="closeall" href="">Close</a>'
            page = new RSInformation({className:linkid, breadcrumb:currLabel, data: data, pagePath: path, popuptext : popuptext, buttons : buttons});
            page.render();			            
            return true;
        }
		
		// menu item is a link
        if(linkid == 'confirm') {
			this.destroy();			
            var path = this.pagePath + '/' + linkid;
            page = new RSSelectMeal({className:linkid, breadcrumb:currLabel, data: this.data, mealsXML: this.mealsXML, pagePath: path});
            page.render();			            
			return true;
        }
		
		if(linkid =="heart") {
			var itemData = this._getMenuItemDatabyTitle("hearthealthy");
			if(itemData) {
				page = new WebSite({className: linkid, breadcrumb: currLabel, data: itemData});
				this.key = '';
				page.render();
			}
			this.destroy();			
            return true;
		}
		
		if(linkid =="kidney") {
			var itemData = this._getMenuItemDatabyTitle("kidneyhealthy");
			if(itemData) {
				page = new WebSite({className: linkid, breadcrumb: currLabel, data: itemData});
				this.key = '';
				page.render();
			}
			this.destroy();			
            return true;
		}

		if(linkid =="liquid") {
			var itemData = this._getMenuItemDatabyTitle("liquiddiet");
			if(itemData) {
				page = new WebSite({className: linkid, breadcrumb: currLabel, data: itemData});
				this.key = '';
				page.render();
			}
			this.destroy();			
            return true;
		}

		
        // handle clicks to somewhere else
        return false;
    },

    renderData: function () {
		
		var context = this;		
		var mealsXML = this.getMeals();				
		var diets = this.getDiets(mealsXML);
		var allergies = this.getAllergies();
		var patientDATA = loadJSON('patient');
		var patientname = patientDATA.userFullName;
		var room = patientDATA.roomNumber;
		
		var dietstring = '<span class="text-color-2">DIET ORDERS:</span><br/>' + diets;
		var allergiesstring = '<span class="text-color-2">FOOD ALLERGIES:</span><br/>' + allergies;
		
		if(this.diets['Regular Diet']) {
			this.$('.page-title').html('Welcome to Patient Room Service');
			this.$('#heading1').html('<p class="text-large">Please confirm that you are:</p>');		
			var buttons = '<a class="footer-button active" id="confirm" href="">Confirm</a><a class="footer-button" id="back" href="">&lt; Back</a><a class="footer-button" id="notme" href="">This is Not Me</a>'
			this.$('.footer-buttons').html(buttons);		
			this.$('#patient').html('<p>' + patientname + ', Room #' + room + '</p>');
			this.$('#diet').html(dietstring);
			this.$('#allergies').html(allergiesstring);
		} else  {
			this.$('.page-title').html('Use your phone to call Room Service<br/>at 24-3463 to order:');
			this.$('#heading1').html('');		
			var buttons = '<a class="footer-button-narrow active" id="back" href="">&lt; Back</a><a class="footer-button" id="heart" href="">Heart Healthy</a><a class="footer-button" id="kidney" href="">Kidney Healthy</a><a class="footer-button" id="liquid" href="">Liquid Diet</a>'
			this.$('.footer-buttons').html(buttons);		
			this.$('#patient').html('<span class="text-color-2">You are on a Specialty Diet. <br/> Use the buttons below for special diet menus.');
			this.$('#diet').html(dietstring);
			this.$('#allergies').html(allergiesstring);		
		}

        // Initialize spatial navigation
        this.$('a').focus(function () {
            context.$('a').removeClass('active');
            $(this).addClass('active');
        });

        SpatialNavigation.init();

		SpatialNavigation.add(this.viewId, {
			selector: '#' + context.id + ' a',
		});
		
        // Focus the first navigable element.
        SpatialNavigation.focus(this.viewId);
    },

    uninit: function () {
        SpatialNavigation.remove(this.viewId);
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/

	getMeals : function ()	{
		var ewf = ewfObject();
		
		 // Get Patient MRN
        var mrn = this.mrn;
        if (!mrn)
            mrn = getMRNDATA();
        this.mrn = mrn;

		var dataobj = {};
        dataobj['room'] = window.settings.room;
        dataobj['bed'] = window.settings.bed;
        dataobj['name'] = window.settings.userFullName;
        var preferredname = '';
        window.settings.mrn = mrn;
        window.settings.vendor = 'Kaiser';		
		dataobj['mrn'] = window.settings.mrn;
		dataobj['vendor'] = window.settings.vendor;
		
		nova.tracker.event('dine', 'start', window.settings.homeID);
		var context = this;
		var url = ewf.dinehost + ":" + ewf.getMeals;
		msg(url)
		var xml = getXdXML(url, dataobj);
		this.mealsXML = xml;
		return xml;

	}, 
	
	getDiets : function (mealsXML)	{
	
	    var meals = [];
        var diets = [];
        var dietstring = '';
        var cnt = 0;
        var MealsResult = '';

        var dietcount = 0;
        var canorderfinal = false;
        var npofinal = false;
        $(mealsXML).find("Person Meals Meal").each(function () {
            var diet = $(this).attr("Diet");
            var id = $(this).attr("Id");

            var canorder = $(this).attr("CanOrder");
            msg(diet.indexOf('NPO'));
            if (diet.indexOf('NPO') >= 0) {
                npofinal = true;
            }

            msg('npo ' + npofinal);
            var npo = $(this).attr("NPO");
            if (canorder == 'Y')
                canorderfinal = true;

            if (typeof diets[diet] == 'undefined' && diet != '') {
                dietstring = dietstring + diet + '<br/>';
                diets[diet] = diet;
            }
            dietcount = dietcount + 1;
        });
		this.diets = diets;
		this.npo = npofinal;
		this.canorder = canorderfinal;		
		return dietstring;
	}, 
	
	getAllergies : function ()	{
		window.settings.totalcarbs = 0;
        var mrn = window.settings.mrn;
        var url = window.portalConfig.getclinical + "?type=allergies&mrn=" + mrn + "&numrec=12&sortorder=asc";
        var dataobj = '';
        var allergies = '';
        var xml = getXdXML(url, dataobj);
        cnt = 0;
        $(xml).find("item").each(function () {
            if (cnt != 0)
                allergies = allergies + ', ';
            allergies = allergies + $(this).find("value").text();
            cnt = cnt + 1;
        });
        allergies = allergies + '</p>';
		return allergies;
	}, 

	_getMenuItemDatabyTitle: function (title) {

        var itemData = null;
        var itemTag = title;        
        
        var items = this.data[this.data.tag];
        if ($.isArray(items)) { // an array of items

            $.each(items, function (i, item) {
                if (item.tag == itemTag) {
                    itemData = item;
                    return false;   // break
                }
            });

        }
        else { // only one item
            if (items.tag == itemTag)
                itemData = items;
        }

        return itemData;
    },
});



