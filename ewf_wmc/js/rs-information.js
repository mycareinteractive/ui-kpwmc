
var RSInformation = View.extend({

    id: 'rs-information',

    template: 'rs-information.html',

    css: 'rs-roomservice.css',
	
	closeall : true,

    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function (key) {
        var $curr = this.$("a.active");
        if (key == 'MENU' || key == 'HOME') {
            this.destroy();
            return true;
        }
        else if (key == 'ENTER' && $curr.hasClass('back-button')) {  // default link click
            this.destroy();
            return true;
        }
        else if (key == 'UP' || key == 'DOWN' || key == 'LEFT' || key == 'RIGHT') { // navigation keys
            SpatialNavigation.move(key.toLowerCase(), this.$('a.active'));
            return true;
        }
        else if (key == 'ENTER') {  // default link click
            this.key = 'ENTER';
            return this.click($curr);
        }
        return false;
    },

    click: function ($jqobj) {
		var data = this.data;
		var linkid = $jqobj.attr('id');	
		
        if (linkid == 'closeall') { // back button
            if (this.closeall == true) {				
                keypressed('CLOSEALL');
                keypressed(216);    //force going back to main menu
            }
			return true;
        }
				
		if (linkid == 'no' || linkid == 'close') { // no/close button, back to previous page
			this.destroy();
			return true;
        }

		if (linkid == 'yes') { // yes button
			clearDineData();
            if (this.closeall == true) {				
                keypressed('CLOSEALL');
                keypressed(216);    //force going back to main menu
            }
			return true;
        }


        // handle clicks to somewhere else
        return false;
    },

    renderData: function () {
		var context = this;

		if(this.pagetitle2)
			this.$('.page-title2').html(this.pagetitle2);		
		if(this.pagetitle)
			this.$('.page-title').html(this.pagetitle);		
		if(this.heading1)
			this.$('#heading1').html(this.heading);		
		if(this.buttons)			
			this.$('.footer-buttons-popup').html(this.buttons);		
		if(this.popuptext)
			this.$('#popuptext').html(this.popuptext);

		
        // Initialize spatial navigation
        this.$('a').focus(function () {
            context.$('a').removeClass('active');
            $(this).addClass('active');
        });

        SpatialNavigation.init();

		SpatialNavigation.add(this.viewId, {
			selector: '#' + context.id + ' a',
		});        // Focus the first navigable element.

        // Focus the first navigable element.
        SpatialNavigation.focus(this.viewId);
    },

    uninit: function () {
        SpatialNavigation.remove(this.viewId);
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
	
});



