function datetime() {
//create the a Formatted Date Time Field

    var d = new Date();
    var dow = d.getDay();
    var hour = d.getHours();
    var minute = d.getMinutes();
    var month = d.getMonth();
    var day = d.getDate();
    var year = d.getFullYear();
    var ampm = 'AM';
    if (day < 10) {
        day = '0' + day;
    }

    if (hour > 12) {
        hour = hour - 12;
        ampm = 'PM';
    }
    if (minute < 10) {
        minute = '0' + minute;
    }

    var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var months = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var datetime = hour + ':' + minute + ' ' + ampm + ' ' + weekday[dow] + ', ' + months[month] + ' ' + day + ', ' + year;

    $("p.datetime").html(datetime);


    return;
}


function msg(message) {
    // Only run on the first time through - reset this function to the appropriate console.log helper
    if (Function.prototype.bind) {
        if (typeof window["Nimbus"] != "undefined") {   // Enseo
            msg = Nimbus.logMessage.bind(Nimbus);
        }
        if (window.console) { // PC
            msg = console.log.bind(console);
        }
    }
    else {
        if (typeof window["Nimbus"] != "undefined") {   // Enseo
            msg = Nimbus.logMessage.apply(this,arguments);
        }
        if (window.console) { // PC
            msg = function () {
                console.log.apply(console, arguments);
            };
        }

    }

    msg.apply(this, arguments);
}


function dec2hex(id) {

    var returns = '';
    var cha = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
    var temp = '';
    while (id > 0) {
        i = id % 16;
        id = Math.floor(id / 16);
        temp = cha[i] + temp;
    }
    returns = temp.toLowerCase();

    return returns;
}

function getHour() {
    var d = new Date();
    var hour = d.getHours();

    return hour;
}


function checkforReload() {
    var ewf = ewfObject();
    var enabled = ewf['autoReload'];
    var reloadtime = ewf['autoReloadHour'] || 3;
    msg('checkforReload...reloadtime: ' + reloadtime);


    if (!enabled || enabled === 'false')
        return;

    var trigger = reloadtime * 1;
    var d = new Date();
    var hour = d.getHours();

    msg('checkforReload...hour/trigger: ' + hour + '/' + trigger);

    if (hour != trigger)
        return;

    var power = checkPower();
    msg('checkforReload...power: ' + power);

    // Force reload even TV is on
    reloadapp(true);
    /*
     if (power=='OFF')	{
     msg('checkforReload...RELOAD');
     reloadapp(true);
     }

     // special logic for KPNW, reload in discharged room even if TV is on
     else if (window.settings.status == '0') {
     msg('checkforReload...RELOAD in discharged room');
     reloadapp(true);
     }
     */
}

function roundNumber(num, dec) {

    var result = num;

    if (num != 0)
        result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);

    if (dec == 2) {
        var resultS = result.toString();
        var resultL = resultS.length;
        var dp = resultS.indexOf('.');
        if (dp == -1) {
            result = resultS + '.00';
        } else {
            var diff = (resultL - 1) - dp;
            if (diff == 1)
                result = resultS + '0';
        }
    }

    return result;
}

function checkObj(object) {

    var stringConstructor = "test".constructor;
    var arrayConstructor = [].constructor;
    var objectConstructor = {}.constructor;

    if (object === null) {
        return "null";
    }
    else if (object === undefined) {
        return "undefined";
    }
    else if (object.constructor === stringConstructor) {
        return "string";
    }
    else if (object.constructor === arrayConstructor) {
        return "array";
    }
    else if (object.constructor === objectConstructor) {
        return "object";
    }
    else {
        return "don't know";
    }

}

function pause(iMilliseconds) {

    var sDialogScript = 'window.setTimeout( function () { window.close(); }, ' + iMilliseconds + ');';

}

function getSettings() {
    var settings = '';
    var images = $("#K_images").text();
    var contrast = $("#K_contrast").text();
    var audio = $("#K_audio").text();

    if (audio)
        settings = audio;
    if (images)
        settings += ' ' + images;
    if (contrast)
        settings += ' ' + contrast;

    return settings;
}

function applySettings(selector) {
    var settings = getSettings();
    $(selector).removeClass('noimages lightdark darklight hearaudio');
    $(selector).addClass(settings);
}

function cacheSettings(noimages, lightdark, darklight, hearaudio) {
    var images = '';
    var contrast = '';
    var audio = '';

    if (noimages)
        images = 'noimages';
    if (lightdark)
        contrast = 'lightdark';
    if (darklight)
        contrast = 'darklight';
    if (hearaudio)
        audio = 'hearaudio';

    $("#K_audio").text(audio);
    $("#K_contrast").text(contrast);
    $("#K_images").text(images);
}


function formatdateTime(format, date) {
    var d = date || new Date();
    return d.format(format);
}

function compareDates(DateA, DateB) {


    var msDateA = new Date(DateA);
    var msDateB = new Date(DateB);

    if (msDateA <= msDateB)
        return -1;  // lt
    else if (msDateA == msDateB)
        return 0;  // eq
    else if (msDateA >= msDateB)
        return 1;  // gt
    else
        return null;  // error
}

function reloadapp(wait) {
    var version = window.settings.version;
    var noWait = !wait;
    if (version == 'ENSEO') {
        Nimbus.reload(noWait);
    }
    else if (version == 'NEBULA') {
        // kill TV player to prevent left-over
        Nebula.getRtspClient().stop();
        Nebula.getTVPlayer().stop(false);
        Nebula.removeCommandHandler(keyHandler);
        if (window.settings.platformVersion > '1.3') {
            Nebula.closeUserBrowser();
        }

        if (window.settings.platformVersion < '2.0') {
            // 1.x Nebula reload has leaks, use restart instead
            Nebula.restart(false);
        }
        else {
            Nebula.reload(wait);
        }
    }
    else if (version == 'PROCENTRIC') {
        // LG doesn't clear cache between reloads, reboot instead
        hcap.power.reboot({
            "onSuccess":function() {
                debug.log("reboot onSuccess");
            },
            "onFailure":function(f) {
                debug.log("onFailure : errorMessage = " + f.errorMessage);
            }
        });

    }
    else {
        window.location.href = '/ewf/';
    }
}

function reboot() {
    nova.tracker.event('device', 'reboot', window.settings.homeID, '', {'sessionControl': 'end'});
    msg('Rebooting device...');
    var version = window.settings.version;
    if (version == 'ENSEO') {
        Nimbus.reboot();
    }
    else if (version == 'NEBULA') {
        // kill TV player to prevent left-over
        Nebula.getRtspClient().stop();
        Nebula.getTVPlayer().stop(false);
        Nebula.removeCommandHandler(KeyHandler);
        Nebula.restart(true);
    }
    else {
        window.location.href = '/ewf/';
    }
}


function discharged()	{
    var page = window.pages.findPage('discharged');
    if(!page) {
        keypressed('CLOSEALL');
    	var page = new Discharged();
        page.render();
    }  		
}

function titleCase(str) {
  str = str.toLowerCase().split(' ');
  for (var i = 0; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
  }
  return str.join(' ');
}


function openGuide(linkid, breadcrumb, data, pagePath) {
	var context = this;
	var page = new TVGuide({
		className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
		oncreate: function () {
		},
		ondestroy: function () {
		}
	});
	page.render();
}


function clearDineData() {
	window.settings.dietorder = '';
	window.settings.meal = '';
	window.settings.deliverytimes = '';		
	window.settings.coursemax = '';		
	window.meals =[];		
}
