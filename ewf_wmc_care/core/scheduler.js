var Ion = Ion || {};

(function ($) {
    
    /**
     * The scheduler is a singleton that manages all periodical activities
     */
    Ion.scheduler = {
        tasks: {},
        
        add: function(name, interval, repeat, fn) {
            if(!name)
                return;
            
            var task = this.tasks[name];    
            if(task) {
                window.clearTimeout(task.id);
                delete task;
            }
            
            var that = this;
            task = {};
            task.interval = interval;
            task.repeat = repeat;
            task.fn = fn;
            task.id = window.setInterval(function() {
                that._
            }, interval);
        }
    };
    
})($);
