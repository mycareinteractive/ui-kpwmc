/**
 * Global functions and helpers in Ion namespace.
 */
var Ion = Ion || {};

(function ($) {
    
    // internal object that holds all global variobles
    Ion._global = {
        deviceID: null,
        ipAddress: null,
        platformVersion: null,
        hashChange: true,
        platform: null,
        rootUrl: '/',
        online: true,
        offlineCount: 0
    };

    /**
     * Log message to console 
     */
    Ion.log = function (message) {
        if(typeof window.console != 'undefined')
            console.log('[WEB - DEBUG] ' + message);
        
        if (Ion.platform() == 'ENSEO' && window.Nimbus)  {
            Nimbus.logMessage('[WEB - DEBUG] ' + message);
        }
    };
    
    /**
     * Log error message to console 
     */
    Ion.error = function (message) {
        if(typeof window.console != 'undefined')
            console.error('[WEB - ERROR] ' + message);
        
        if (Ion.platform() == 'ENSEO' && window.Nimbus)  {
            Nimbus.logMessage('[WEB - ERROR] ' + message);
        }
    };
    
    /**
     * Determine what platform the code is running on. 
     */ 
    Ion.platform = function () {
        if (Ion._global.platform)
            return Ion._global.platform;
        
        if (typeof window['EONimbus'] != 'undefined') {
            Ion._global.platform = 'ENSEO';
        }
        else if (typeof window['EONebula'] != 'undefined') {
            Ion._global.platform = 'NEBULA';
        }
        else if (location.search.indexOf('PollingIP')>=0) {
            Ion._global.platform = 'TCM';
        }
        else if (navigator.userAgent.indexOf(';LGE ;') > -1) {
            Ion._global.platform = 'PROCENTRIC';
        }
        else {
            Ion._global.platform = 'DEV';
        }
        
        return Ion._global.platform;    
    };
    
    /**
     * Retrieve the query string in current URL and parse into a dictionary.
     * @return {Object}     Return an object with unescaped key/value pairs mapped to the query attributes. 
     */
    Ion.queries = function () {
        var result = {}, keyValuePairs = location.search.slice(1).split('&');
    
        keyValuePairs.forEach(function(keyValuePair) {
            keyValuePair = keyValuePair.split('=');
            result[unescape(keyValuePair[0])] = unescape(keyValuePair[1]) || '';
        });
    
        return result;
    };
    
    /**
     * Whether the app will poll the URL for hash value changes.
     * This is vital for mouse-drive platforms such as Nebula.
     * For TCM and Enseo platforms this is optional because the navigation is keyboard/remote based.
     * On Enseo platform we should disable this feature for performance issue.
     */
    Ion.hashChange = function () { 
        return Ion._global.hashChange; 
    };

    
    /**
     * Root URL of the application.  We will automatically set this value. 
     */
    Ion.rootUrl = function () { 
        return Ion._global.rootUrl; 
    };
    
    /**
     * Whether app is online 
     */
    Ion.online = function () {
        return Ion._global.online;
    };
    
    /**
     * Device ID of the hardware.
     * For Enseo and Nebula this is the MAC address.
     * For TCM this is the 'device-id' value in the URL.
     * For DEV we use 000000000000 (12 digits).
     */
    Ion.deviceID = function () {
        if (Ion._global.deviceID)
            return Ion._global.deviceID;
            
        if (Ion.platform() == 'ENSEO') {
            var device = Nimbus.getNetworkDeviceList()[0] || '';
            var interf = Nimbus.getNetworkDevice(device);
            Ion._global.deviceID = interf.getMAC().replace( /:/gi, '' );
            Ion._global.ipAddress = interf.getIP();
        }
        else if (Ion.platform() == 'NEBULA') {
            var device = Nebula.getNetworkDeviceList().pop();
            var interf = Nebula.getNetworkDevice(device);
            Ion._global.deviceID = interf.getMAC().replace( /:/gi, '' );
            Ion._global.ipAddress = interf.getIP();
        }
        else if (Ion.platform() == 'TCM') {
            var queries = Ion.queries();
            Ion._global.deviceID = queries['device-id'];
        }
        else if (Ion.platform() == 'PROCENTRIC') {
            var device = window.LG.getNetworkDevice(0);
            // get mac
            Ion._global.deviceID = device.getMac().replace(/:/gi, '');
            // get ip
            Ion._global.ipAddress = device.getIp();
        }
        else {
            var config = Ion.fetcher.getPortalConfigData();
            var tmac = (typeof config.browser != 'undefined' && typeof config.browser.mac != 'undefined') ? config.browser.mac : "0021F80484B1";
            // Ion._global.deviceID = '0021F8043327'; //room 102
			
			//Ion._global.deviceID = '00045F905DD3'; //room 103
			Ion._global.deviceID = tmac;
			//Ion._global.deviceID = '0021F8015156'; //room 100
			//Ion._global.deviceID = '0021F80430BF'; //room 370
			//Ion._global.deviceID = '0021F80430BF'; //room 501
			//Ion._global.deviceID = '0021F8043364'; //room 605
        }
        return Ion._global.deviceID;
    };
    
    Ion.ipAddress = function () {
        if (Ion._global.ipAddress)
            return Ion._global.ipAddress;
        Ion.deviceID();
        return Ion._global.ipAddress || 'Unknown';
    };
    
    Ion.platformVersion = function () {
        if (Ion._global.platformVersion)
            return Ion._global.platformVersion;
        
        if (Ion.platform() == 'ENSEO') {
            Ion._global.platformVersion = Nimbus.getFirmwareRevision() || 'No Firmware';
        }
        else if (Ion.platform() == 'NEBULA') {
            Ion._global.platformVersion = Nebula.getAPIRevision() || 'Invalid Nebula';
        }
        else if (Ion.platform() == 'PROCENTRIC') {
            Ion._global.platformVersion = window.LG.getPlatformVersion() || 'No Firmware';
        }
        return Ion._global.platformVersion || 'Unknown';
    };
    
    Ion.handshake = function() {
        if (Ion.platform() == 'ENSEO') {
            Nimbus.setHandshakeTimeout(120);
            Nimbus.handshake(Ion.APP_NAME + ' ' + Ion.APP_VERSION);
        }
        return;
    };
    
    /**
     * Dynamically load an external javascript file 
     * @param {Object} url - the URL of the script file.
     */
    Ion.loadScript = function(url) {
        return jQuery.ajax({
            cache: true,
            crossDomain: true,
            dataType: "script",
            url: url
        });
    };
    
    /**
     * 
     * @param {Object} format reference http://blog.stevenlevithan.com/archives/date-time-format
     * @param {Object} date optional Date object
     */
    Ion.dateTime = function(format, date) {
        var d = date || new Date();
        return d.format(format);
    };



   /**
     * 
     * @param {Object} format reference http://blog.stevenlevithan.com/archives/date-time-format
     * @param {Object} date optional Date object
     */
    Ion.compareDates = function(DateA, DateB) {
 
    
    var msDateA = new Date(DateA);
    var msDateB = new Date(DateB);

    if (msDateA <= msDateB)
      return -1;  // lt
    else if (msDateA == msDateB)
      return 0;  // eq
    else if (msDateA >= msDateB)
      return 1;  // gt
    else
      return null;  // error
	};

    
    // load platform specific scripts
    _loadPlatformScripts();
    
    function _loadPlatformScripts() {
        if (Ion.platform() == 'ENSEO') {
            $.when(
                Ion.loadScript('./libs/nimbus/enseo_nimbus.js'),
                Ion.loadScript('./libs/nimbus/enseo_appfile.js'),
                Ion.loadScript('./libs/nimbus/enseo_applog.js'),
                Ion.loadScript('./libs/nimbus/enseo_browser.js'),
                Ion.loadScript('./libs/nimbus/enseo_datachan.js'),
                Ion.loadScript('./libs/nimbus/enseo_player.js'),
                Ion.loadScript('./libs/nimbus/enseo_tvcontroller.js'),
                Ion.loadScript('./libs/nimbus/enseo_nimbus_child_frame.js')
            ).done(function(){
                $.holdReady(false);
            });
            
        }
        else if (Ion.platform() == "NEBULA") {
            Ion.loadScript('./libs/nebula/nebula.js').done(function(){
                $.holdReady(false);
            });
        }
        else if (Ion.platform() == "TCM") {
            Ion.loadScript('./libs/tcm/tcm.js').done(function(){
                $.holdReady(false);
            });
        }
        else if (Ion.platform() == "PROCENTRIC") {
            Ion.log('[global.js Ion.platform()] ' + Ion.platform());
            $.holdReady(false);

        }
        else if (Ion.platform() == "DEV") {
            Ion.loadScript('./libs/dev/dev.js').done(function(){
                $.holdReady(false);
            });
        }
    }
    
    
    
})($);


