var Ion = Ion || {};

(function ($) {

    /**
     * The navigator is a singleton that manages the boot process and page transitions
     */
    Ion.navigator = {

        // public properties

        /**
         * The current main content view
         */
        currentView: null,

        /**
         * The optional global dock/launch bar view.
         */
        dockView: null,

        // private properties

        // public functions

        /**
         * Start the navigator. This function will kick off the boot process.
         */
        start: function () {
            var self = this;
            window.portalConfig = Ion.fetcher.getPortalConfigData(true);
            if (!window.portalConfig) {
                // No config data.
                this.reload(60 * 1000, 'Failed to initialize app.');
                return;
            }

            // reset hash to nothing
            location.hash = '';

            // start history tracking
            Backbone.history.start({
                hashChange: Ion.hashChange(),
                root: Ion.rootUrl(),
                silent: true
            });

            // Init
            this._initialize().done(function () {
                // start event loop
                Ion.dispatcher.initialize();
                self._startPollers();
                $('div#loading').hide();
            });

        },

        /**
         * Restart the app. On ENSEO platform this also triggers browser reset.
         */
        reload: function (delay, message) {
            $('div#loading').show();
            if (!delay || typeof delay != 'number' || delay < 1000) {
                if (Ion.platform() == "ENSEO") {
                    Nimbus.reload(true);
                }
                else if (Ion.platform() == "PROCENTRIC") {
                    // LG doesn't clear cache between reloads, reboot instead
                    hcap.power.reboot({
                        "onSuccess": function () {
                            Ion.log("reboot onSuccess");
                        },
                        "onFailure": function (f) {
                            Ion.log("onFailure : errorMessage = " + f.errorMessage);
                        }
                    });
                }
                else {
                    location.href = '/ewf/';
                }
            }
            else {
                if (message)
                    Ion.navigator.progress(message + ' Reload app in ' + delay / 1000 + ' seonds...');
                setTimeout(function () {
                    Ion.navigator.reload(delay - 1000, message);
                }, 1000);
            }
        },

        progress: function (message, logging) {
            $('div#loadinginfo').html('<p>' + message + '</p>');
            if (logging)
                Ion.log(message);
        },

        debugInfo: function (status) {
            var debug = $('div#debuginfo');
            var str = '<h1>Info</h1><ul>';
            str += '<li><h2>Core version</h2><p>' + Ion.APP_VERSION + '</p></li>';
            str += '<li><h2>Platform</h2><p>' + Ion.platform() + '</p></li>';
            debug.html(str);
            debug.show().delay(10000).hide('fast');
        },

        display: function (view, data, options) {
            var context = this;
            if (this.currentView) {
                this.currentView.destroy();
            }

            this.currentView = view;

            this.currentView.show(data, function () {
                $("div#content div#current").html(context.currentView.el);
                $('#revision').text(window.package.name + ' ' + window.package.version);
            });
        },

        authenticate: function () {
            // user data
            var oldUserData = Ion.fetcher.getUserData(false);
            var promise = Ion.fetcher.getUserData(true, true).done(function (data) {
                // get global cache user data, which is already parsed
                var userData = Ion.fetcher.getUserData(false);
                window.portalUserData = userData;

                if (!Ion._global.online) { // server not available
                    Ion._global.offlineCount++;
                    if (Ion._global.offlineCount >= 5) {
                        this.reload(60 * 1000, 'Failed to authenticate user.');
                        return false;
                    }
                    return true;
                }

                try {
                    // Admitted room have TV number, load the config override
                    if (userData && userData.home && userData.home.status == '1') {
                        window.portalConfig = Ion.fetcher.loadConfigOverride();
                    }

                    Ion._global.offlineCount = 0;

                    if (!userData.home) {
                        // Not provisioned, go to provision page.
                        location.href = '/ewf/';
                    }

                    // If room status changed, we will reload regardless
                    if (oldUserData && oldUserData.home) {
                        if (oldUserData.home.status != userData.home.status) {
                            Ion.navigator.reload();
                            return true;
                        }
                    }

                    if (userData.home.status != '1') {
                        // Discharged or vacant room/bed, go to discharged page.
                        Ion.router.navigate('discharged', {trigger: true, replace: true});
                    }
                    /*else if (!userData.home.language) {
                        // Patient admitted but didn't accept on UpCare acceptance page yet
                        Ion.router.navigate('decline', {trigger: true, replace: true});
                    }*/
                    else {
                        // all good, load main page
                        var page = window.portalConfig.mainPage || 'careboard';
                        Ion.router.navigate(page, {trigger: true, replace: true});
                    }
                }
                catch (e) {
                    Ion.error('Exception caught when authenticating: ' + e);
                    return true;
                }
            });
            return promise;
        },

        // initialize the app
        _initialize: function () {
            var self = this;
            Ion.deviceID();

            if (Ion.platform() == 'ENSEO') {
                // make hash change detection really slow for performance reason   
                Backbone.history.interval = 30000;
                Nimbus.setLogLevel("Nimbus", 2);
                Nimbus.setLogLevel("Lib", 2);
                Nimbus.setHandshakeTimeout(0);
                Nimbus.setTimeZoneMode('Name');
                Nimbus.setTimeZoneName(Ion.APP_TIMEZONE, Ion.APP_TIMEZONE_NAME);
                Nimbus.setNetworkAPIEnable(true);           // Turn on NetAPI
                Nimbus.setNetworkAPIAllCallsEnable(true);   // Enable all NetAPI calls
                Nimbus.setAutoSpacialNavigationEnable(false);   // disable Opera keyboard nav
                // Set to false rather than comment it, so all boxes shut off the update
                Nimbus.setIPAutoUpdateParameters(false, '239.255.11.103', '8000');
                // Enable console and telnet debugging
                Nimbus.setConsoleEnabled(true, true);
                Nimbus.setTelnetEnabled(true, true);
                var TVController = Nimbus.getTVController();
                if (TVController) {
                    TVController.setVolume(10);
                }
                Ion.handshake();
                setInterval("Ion.handshake()", 30000);
            }
            else if (Ion.platform() == 'NEBULA') {
            }
            else if (Ion.platform() == 'TCM') {
            }
            else if (Ion.platform() == 'PROCENTRIC') {
                var pc = Ion.fetcher.getPortalConfigData();
                Ion.log('[index.html bootstrap] Time Object URL: ' + pc.proxyServerUrl + '/time');

                $.getScript(pc.proxyServerUrl + '/time', function () {
                    LG.setTime(window.serverTime);
                });
            }
            else {
            }

            // App root URL
            Ion._global.rootUrl = location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1);

            // user data, synchronous calls
            this.progress('Authenticating...', true);
            return this.authenticate().done(function () {
                if (Ion.platform() != 'DEV') {
                    self.progress('Reporting hardware info...', true);
                    Ion.fetcher.sendPlatformInfoData(true, true); // async
                }
            });
        },

        // setup scheduled jobs such as auto-reload every so often, check for discharge...etc 
        _startPollers: function () {
            var interval = 60000;
            var context = this;

            var config = Ion.fetcher.getPortalConfigData();

            // polling for user data and discharge
            interval = config.checkUserDataInterval || 600000;
            var userDataRetry = 0;
            $.doTimeout('userdata polling', interval, function () {
                Ion.navigator.authenticate();
                return true;
            });

            // auto sleep poller
            interval = 120 * 1000;
            $.doTimeout('auto sleep and reload', interval, function () {
                context._autoSleep();
                return true;
            });

            // check daily reload every 59 minutes.  If configuration is 3:00 then device will reload between 3:00 and 3:59
            window.setInterval(function () {
                context._dailyReload();
            }, 59 * 60 * 1000);
        },

        // check for daily reload
        _dailyReload: function () {
            var config = Ion.fetcher.getPortalConfigData();

            var now = new Date();
            var hour = now.getHours();

            var reload = config.autoReloadAt || '3:00';
            var reloadHour = Number(reload.split(':')[0]);

            Ion.log('Checking daily reload time...');

            // reload
            if (config.autoReload && config.autoReload != false && config.autoReload != 'false') {
                if (hour == reloadHour) {
                    Ion.log('***********************************************');
                    Ion.log('Daily reload time met, reloading...');
                    Ion.log('***********************************************');
                    Ion.navigator.reload();
                }
            }
        },

        // check for auto sleep function
        _autoSleep: function () {
            var config = Ion.fetcher.getPortalConfigData();

            var now = new Date();
            var hour = now.getHours();
            var minute = now.getMinutes();

            var from = config.autoSleepFrom || '22:00';
            var fromHour = Number(from.split(':')[0]);
            var fromMinute = Number(from.split(':')[1]);

            var to = config.autoSleepTo || '6:00';
            var toHour = Number(to.split(':')[0]);
            var toMinute = Number(to.split(':')[1]);

            Ion.log('Checking auto sleep time...');

            // auto sleep
            if (config.autoSleep && config.autoSleep != false && config.autoSleep != 'false') {

                var state = null;

                // We only auto-turn on/off TV within 10 minute window of the sleep/wakeup time.
                // If someone forces the TV on/off after 10 minute past sleep/wakeup time, they can overwrite the feature.
                if (hour == fromHour && (minute - fromMinute) >= 0 && (minute - fromMinute) <= 10) {
                    Ion.log('***********************************************');
                    Ion.log('Auto sleep time is met, turning TV off...');
                    Ion.log('***********************************************');
                    state = false;
                }
                else if (hour == toHour && (minute - toMinute) >= 0 && (minute - toMinute) <= 10) {
                    Ion.log('***********************************************');
                    Ion.log('Auto wakeup time is met, turning TV on...');
                    Ion.log('***********************************************');
                    state = true;
                }

                if (Ion.platform() == 'ENSEO') {
                    var tv = Nimbus.getTVController();
                    if (tv && state != null) {
                        tv.setPower(state);
                    }
                }
                else if (Ion.platform() == 'PROCENTRIC') {
                    if (state != null) {
                        var pstate = (state == false) ? 'warm' : 'normal';
                        window.LG.setPower(pstate);
                    }
                }
            }
        }


    }; // Ion.navigator


})($);
