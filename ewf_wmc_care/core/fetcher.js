var Ion = Ion || {};

(function ($) {
    /**
     * The fetcher is a singleton that manages the data fetch and data mining 
     */
    var MAIN_METHOD = 'attribute';
    var PROXY_METHOD = 'wsdl';
    var ATTR_CHANNEL_GROUP =    'json_libs_clu_get_region_channel_group';
    var ATTR_ACCOUNT_REVIEW =   'json_libs_oss_account_review_for_one_guest';
    var ATTR_HOME_STATUS =      'json_libs_oss_check_home_status';
    var ATTR_CHECK_SERVICE =    'json_libs_oss_check_service_entitlement';
    var ATTR_USER_DATA =        'json_libs_oss_get_user_data';
    var ATTR_LIST_GUEST =       'json_libs_oss_list_guest_data';
    var ATTR_STB_INFO =         'json_libs_oss_stb_info_setting';
    var ATTR_GET_PRODUCT =      'json_libs_ote_get_product_offering';
    var ATTR_SERVER_LOAD =      'json_libs_ote_get_server_load_info';
    var ATTR_LIST_TICKET =      'json_libs_ote_list_ticket';
    var ATTR_PURCHASE =         'json_libs_ote_purchase_product';
    var ATTR_GET_ASSET =        'json_libs_stb_get_asset';
    var ATTR_LIST_ENTRY =       'json_libs_stb_list_entry';
    var ATTR_STB_INFO =         'json_libs_oss_stb_info_setting';
    var ATTR_LIST_GUEST_DATA =  'json_libs_oss_list_guest_data';
    var URL_PORTAL_CONFIG =     'http://siteintegration.aceso.com:9080/ams/aceso/getPortalConfigJson';
    var URL_PORTAL_MENU =       'http://siteintegration.aceso.com:9080/ams/aceso/getMenuXml';
    var URL_USER_CONFIG =       'http://siteintegration.aceso.com:9080/ams/aceso/getConfigSelections';
    var URL_USER_CONFIG_ADD =   'http://siteintegration.aceso.com:9080/ams/aceso/addConfigSelections';
    var URL_AGENDA =            'http://siteintegration.aceso.com:9080/ams/aceso/getAgendaJson';

    Ion.fetcher = {
        
        // private properties
        _userData: {},
        _portalConfigData: null,
        _menuData: null,
        
        // public functions
        
        
        ajax: function (url, dataobj, async, nocache, type, method) {
            var cache = !nocache;
            async = async || false;
            var retdata = null;
            
            var promise = $.ajax({
                async: async,
                type: method,
                cache: cache,
                url: url,
                data: dataobj,
                dataType: type,
                dataFilter: function (data, type) {
                    if (type == 'json' && url.indexOf('stbservlet') != -1) return Ion.fetcher._cleanJSON(data);
                    return data;
                },
                beforeSend: function (jqXHR, settings) {
                    Ion.log('>> ' + method + ' "' + this.url + '"...');
                },
                complete: function (jqXHR, textStatus) {
                    
                },
                success: function (data, textStatus, jqXHR) {
                    retdata = data;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Ion.error('<< ' + errorThrown + ' ' + textStatus + ' "' + this.url + '"');
                },
                timeout: 10000
            });
            
            if(async)
                return promise;
            else
                return retdata;
        },
        
        /**
         * Handy JSON call function.
         * The response is automatically filtered to ensure valid JSON
         * For synchronize/block call, pass 'true' to sync parameter
         * @example
         * Ion.fetcher.getJSON('/stbservlet', {
         *      attribute: 'json_libs_oss_get_user_data',
         *      device-id: '01234567'
         *  })
         */
        getJSON: function (url, dataobj, async, nocache) {
            return this.ajax(url, dataobj, async, nocache, 'json', 'GET');
        },
        
        /**
         * Handy Cross-Domain JSON call shortcut fucntion.
         * For synchronize/block call, pass 'true' to sync parameter
         * @exmaple 
         * Ion.fetcher.getXdJSON('http://192.168.100.10:9080/ams/aceso/getMenuJson', {
         *      portal: 'srh',
         *      bed: 1
         *  })
         * 
         * This request will be converted to 
         * /Proxy/ServerProxy?wsdl=http://192.168.100.10:9080/ams/aceso/?portal=srh&bed=1 
         */
        getXdJSON: function (xdurl, dataobj, async, nocache) {
            var url = window.portalConfig.host + window.portalConfig.proxy;
            var dataString = PROXY_METHOD + '=' + xdurl + '?' + $.param(dataobj);
            return this.getJSON(url, dataString, async, nocache);
        },
        
        /**
         * Handy XML call shortcut function.
         * @example
         * Ion.fetcher.getXML('/stbservlet', {
         *      attribute: 'oss_get_user_data',
         *      device-id: '01234567'
         *  })
         */
        getXML: function (url, dataobj, async, nocache) {
            return this.ajax(url, dataobj, async, nocache, 'xml', 'GET');    
        },
        
        /**
         * Handy Cross-Domain XML call shortcut fucntion.
         * @exmaple 
         * Ion.fetcher.getXdXML('http://192.168.100.10:9080/ams/aceso/getMenuXml', {
         *      portal: 'srh',
         *      bed: 1
         *  })
         */
        getXdXML: function (xdurl, dataobj, async, nocache) {
            var url = window.portalConfig.host + window.portalConfig.proxy;
           
            var dataString = PROXY_METHOD + '=' + xdurl + '?' + $.param(dataobj);
            return this.getXML(url, dataString, async, nocache);
        },
       
        /**
         * Get user, home and device data.
         * When cached data is available the cached data will be returned.
         * If not available we will make a call to server API "oss_get_user_data".
         * If forceupdate is true, a call to server is guaranteed even if cached data is available.
         * If you use asynchronous version you will need to chain a deferred object.
         * @param {Boolean} forceupdate     If true, an ajax call to server is forced to make sure data is update to date.
         * @param {Boolean} async    Whether using Asynchronous call. Synchronous code is easier to manage but will block browser.
         */
        getUserData: function (forceupdate, async) {
            if(!forceupdate) {
                return this._userData;  // cached data
            }
            
            var args = {};
            args[MAIN_METHOD] = ATTR_USER_DATA;
            args['device_id'] = Ion.deviceID();
            
            var url = window.portalConfig.host + window.portalConfig.method;
            var ret = this.getJSON(url, args, async, true);
            if(!async) {
                return this._parseUserData(ret);
            }
            else {
                var promise = ret.done( function(userData) {
                    Ion.fetcher._parseUserData(userData);
                });
                return promise;                
            }
        },
        
        /**
         * Portal based configuration values.
         * These values are returned from middleware server.
         * NOTICE: In order for the config webservice to work, we need a host item in HSDB hosts file:
         * x.x.x.x  siteintegration.aceso.com
         * This is because before we get config we dont even know the middleware server address.
         * @param {Boolean} forceupdate     If true, an ajax call to server is forced to make sure data is update to date.
         * @param {Boolean} async    Whether using Asynchronous call. Synchronous code is easier to manage but will block browser.
         */
        getPortalConfigData: function (forceupdate, async) {
            if(!forceupdate && this._portalConfigData) {
                return this._portalConfigData;
            }
            
            var homeData = Ion.fetcher.getUserData().home || {};
            var portalID = homeData.portalID || '';
            var args = { portal: portalID };

            // get app version
            var packageJson = this.getJSON('./package.json', null);
            window.package = packageJson || {};
            window.package.version = (packageJson.version || '0.0.0.0').replace('$(COMMIT)', '0').replace('$(BUILD)', '0');
            
            // We try load an override(dev) portalconfig.json first.  If not there we will load production version
			var ret = this.getJSON('./portalconfig.json', null);
			
            if(!async) {
                var data = ret;
                if(data && data.response) {                    
                    this._portalConfigData = data.response;
                }
                else {
                    Ion.log('No portalconfig.json found, try local protalconfig.prod.json file.');
                    var url = './portalconfig.prod.json';
                    var data = this.getJSON(url, null);
                    if(data && data.response) {
                        this._portalConfigData = data.response;
                    }
                    else {
                        Ion.error('Failed to get local portal config cache file.');
                    }
                }                
                return this._portalConfigData;
            }
            else {
                var promise = ret.done( function(data) {
                    if(!data.response)
                        Ion.error('Failed to get portal config data.');
                    Ion.fetcher._portalConfigData = data.response;
                });
                return promise;
            }
        },
        
        loadConfigOverride: function () {        
            // in portalConfig.json we allow a special section "overrideByTVNumber" to hold specific config for each
            // TV location.  Eg: TV A will have regular Ed library while TV B will have a Mother/Baby ED library.                       
            var config = window.portalConfig;
            var userData = window.portalUserData;
            var override = config.overrideByTvNumber;            
            if(userData.home.status != '1')
                 return config;
            
            var tvNumber = userData.device.TVNumber;
            if (!tvNumber || !override || !override[tvNumber])
                return config;
            $.extend(config, override[tvNumber]);   
            return config;
        },


        sendPlatformInfoData: function(forceupdate, async) {
            var args = {};
            args[MAIN_METHOD] = ATTR_STB_INFO;
            args['device_id'] = Ion.deviceID();
            args['stb_ip'] = Ion.ipAddress();
            args['firmware'] = Ion.platformVersion();
            var url = window.portalConfig.host + window.portalConfig.method;
            var ret = this.getJSON(url, args, async, true);
        },
        
        getMRN: function(forceupdate, async)	{
            var args = {};
            var userData = Ion.fetcher.getUserData();
            args[MAIN_METHOD] = ATTR_LIST_GUEST_DATA;
            args['home_id'] = userData.home.homeID.toUpperCase();
            var url = window.portalConfig.host + window.portalConfig.method;
            var ret = this.getJSON(url, args, async, true);
            if(!async) {
                var mrn = '';
                if(ret && ret.DataArea && ret.DataArea.ListOfBill 
                    && ret.DataArea.ListOfBill[0] && ret.DataArea.ListOfBill[0].tagAttribute) {
                    mrn = ret.DataArea.ListOfBill[0].tagAttribute.accountNumber;
                }
                return mrn;
            }
            else {
                var promise = ret;
                return promise;                
            }
        },

        
        // private functions
    
        _cleanJSON: function (data) {
            data = data.replace(/\\/g, '&#47;');    // convert all backslashes to slashes in SeaChange response
            data = data.replace(/(\r\n|\n|\r)/gm,""); // remove all line breaks
            return data;
        },
        
        _parseUserData: function (userData) {
            var data = { home: null, patient: null, device: null };
            
            if(!userData || !userData.DataArea) {
                // server or connection error
                Ion._global.online = false;
                Ion.error('Invalid user data: ' + JSON.stringify(userData));
                return data;
            }
            
            Ion._global.online = true;
            if(!userData.DataArea.length) {
                // not provisioned
                data.home = null;
            }
            else if(userData.DataArea && userData.DataArea.length>0) {
                var dataItem = userData.DataArea[0];
                
                data.home = dataItem.tagAttribute;
                
                var pos = dataItem.tagAttribute.homeID.indexOf("_");    // get room and bed from homeID
                if(pos>=0) {                        
                    data.home['roomNumber'] = dataItem.tagAttribute.homeID.substr(3,pos-3);
                    data.home['roomBed'] = dataItem.tagAttribute.homeID.substr(pos+1);                    
                }
                
                if(dataItem.ListOfDevice && dataItem.ListOfDevice.length>0) {
                    data.device = dataItem.ListOfDevice[0].tagAttribute;
                }
                
                if(dataItem.ListOfSTBUser && dataItem.ListOfSTBUser.length>0) {
                    data.patient = dataItem.ListOfSTBUser[0].tagAttribute;
                }
            }
            
            Ion.fetcher._userData = data;
            return data;
        }
        
    }; // Ion.fetcher
    
            
    

})($);
