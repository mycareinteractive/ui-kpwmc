debug = {

    trace: function (msg) {
      jQuery("#dbgOut").append("<p>" + msg + "</p>");
    },

    log: function (msg) {
        var resp = '[Aceso - Web] ' + msg;
        console.log(resp);
        //return true;
    },

    status: function(msg){
        var li = $("<li />");
        li.append(msg);
        $("#status").prepend(li);
    }

};