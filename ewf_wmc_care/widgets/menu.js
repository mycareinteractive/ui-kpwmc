/**
 * Menu this
 * Menu is a list of items (usually <a>) that user can use either keyboard
 * to scroll, navigate and activate.
 */
var Ion = Ion || {};
Ion.Widget = Ion.Widget || {};

(function ($) {
    
    Ion.Widget.Menu = Backbone.View.extend({
        // common Widget properties
        el: 'div#menuwrapper',  //eg: mainmenu
        
        // Menu properties
        itemSelector: 'a, div[data-role="button"]',
        nextKey: 'DOWN',
        prevKey: 'UP',
        activeClass: 'active',
        
        // callbacks
        onclick: null,
        onfocus: null,
        onblur: null,
        
        // methods
        initialize: function() {
            _.extend(this, this.options);
        },
        
        process: function(type, msg, data) {
            // Currently Menu widget only process prevKey, nextKey and ENTER.
            // Mouse event is driven by View directly.
            if(type == 'COMMAND') {
                return this._processCommand(msg, data);
            }
            return false;
        },
        
        _processCommand: function(key, data) {
            var keys = [this.prevKey, this.nextKey, 'ENTER'];
            if(keys.indexOf(key) < 0)
                return false;
                
            var $curr = this.$el.children(this.itemSelector + '.' + this.activeClass)
            var $next = $curr;
            
            // on 'ENTER', we trigger onclick function and that's it
            if(key == 'ENTER') {
                if($curr.length==1 && this.onclick) {
                    return this.onclick($curr);
                }
            }
            
            // move the focus around
            if(key == this.nextKey) {
                $next = $curr.nextAll(this.itemSelector).first();
                    
                if($next.length <= 0)
                    $next = this.$el.children(this.itemSelector + ':first-child').first();
            }
            else if(key == this.prevKey) {
                $next = $curr.prevAll(this.itemSelector).first();
                    
                if($next.length <= 0)
                    $next = this.$el.children(this.itemSelector + ':last-child').first();
            }
            else {
                return false;
            }
            
            $curr.removeClass(this.activeClass);
            if(this.onblur)
                this.onblur($curr);
            $next.addClass(this.activeClass);
            if(this.onfocus)
                return this.onfocus($next);
                
            return false;
        }
    });
    
})($);