/**
 * Careboard view module.
 */
var CareboardView = Ion.View.extend({

    template: 'careboard.html',
    css: 'careboard.css',
    id: 'careboard',


    render: function () {
        this.addWidget('clock', new Ion.Widget.Clock({
            el: this.$('#date .header2 p')[0],
            format: 'dddd, mmm d - h:MM TT'
        }));
        var context = this;
        // schedule a timer every 60 seconds.
        $.doTimeout('careboard polling', 120000, function () {
            try {
                context._updateData();
            }
            catch (e) {
                Ion.error('Exception caught when updating data: ' + e);
            }
            return true;
        });
        // delay 1 second and do an one-time update
        $.doTimeout('careboard polling once', 500, function () {
            try {
                context._updateData();
            }
            catch (e) {
                Ion.error('Exception caught when updating data: ' + e);
            }
            return false;
        });

        return this;
    },

    uninit: function () {
        $.doTimeout('careboard polling');   // stop the timer
    },

    click: function ($obj) {
        Ion.log("** " + $obj.attr('id') + ' clicked **');
        return false;
    },

    _updateData: function () {

        var self = this;

        //get data from middleware
        var userData = Ion.fetcher.getUserData();

        var cnt = '';
        // Get Patient MRN
        var url = '', dataobj = {};
        var preferredname = '';
        var mrn = '';

        var x0 = Ion.fetcher.getMRN(true, true).done(function (ret) {
            if (ret && ret.DataArea && ret.DataArea.ListOfBill
                && ret.DataArea.ListOfBill[0] && ret.DataArea.ListOfBill[0].tagAttribute) {
                mrn = ret.DataArea.ListOfBill[0].tagAttribute.accountNumber;
            }
        });

        $.when(x0).done(function () {
            var main_url = window.portalConfig.getclinical;

            // Get Patient Preferred Name
            url = main_url + "type=patient&mrn=" + mrn + "&numrec=3&sortorder=asc";

            var dataobj = '';
            var x1 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    preferredname = ($(this).find("value").text());
                });
            });

            // Get Estimated Discharge Date
            url = main_url + "type=estimateddisdate&mrn=" + mrn + "&numrec=1&sortorder=asc";
            var estimateddisdate = '';
            var dataobj = '';
            var x2 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    var ed = ($(this).find("value").text());
                    if (ed != '') {
                        estimateddisdate = new Date(ed.substr(0, 4) + "/" + ed.substr(4, 2) + "/" + ed.substr(6, 2))
                        estimateddisdate = estimateddisdate.format("mm-dd-yyyy");
                    }
                });
            });


            // Get Goals
            url = main_url + "type=goals&mrn=" + mrn + "&numrec=6&sortorder=asc";
            var dataobj = '';
            var goals = '';
            var checktest = new Array;
            var code = '';
            var x3 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    desc = ($(this).find("codedescription").text());
                    value = ($(this).find("value").text());
                    code = ($(this).find("code").text());
                    status = ($(this).find("orderStatus").text());
                    value = value.replace(/\;/g, '<br/>');
                    value = value.replace(/\:/g, '<br/>');
                    value = value.replace(/\,/g, '<br/>');
					goals = goals + value; 
                });
            });

            // Get Careteam Data
            url = main_url + "type=careteam&mrn=" + mrn + "&numrec=100&sortorder=asc";
            var dataobj = '';
            var enddate = '';
            var careteam = '';
            cnt = 0;
            var maxcareteam = 10;
            var x4 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                //Ion.log(xml);
                $(xml).find("item").each(function () {
                    if (cnt <= maxcareteam) {
                        desc = ($(this).find("codedescription").text());
                        value = ($(this).find("value").text());
                        enddate = ($(this).find("enddate").text());
                        var now = Ion.dateTime("yyyy-mm-dd HH:MM:ss");
                        var datecompare = 0;
                        if (enddate.length >= 1)
                            datecompare = Ion.compareDates(enddate, now);
                        //Ion.log(datecompare + ' ' + value + ' ' + now + ' ' + enddate);
                        if (datecompare >= 0) {
                            if (desc != '' && desc.length >= 1) {
                                careteam = careteam + desc + ':<span class="text-color-1">' + value + '</span><br/>';
                            } else {
                                careteam = careteam + value + '<br/>';
                            }
                            cnt = cnt + 1;
                        }
                    }
                });
            });

            // Get Activity
            url = main_url + "type=activity&mrn=" + mrn + "&numrec=6&sortorder=asc";
            var dataobj = '';
            var activity = '';            
            var code = '';
            var x5 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
                    desc = ($(this).find("codedescription").text());
                    value = ($(this).find("value").text());
                    code = ($(this).find("code").text());
                    status = ($(this).find("orderStatus").text());
                    value = value.replace(/\;/g, '<br/>');
                    value = value.replace(/\:/g, '<br/>');
                    value = value.replace(/\,/g, '<br/>');
					if(code == 'ACES-5937' && (value.length>=1)) 
						value = "Staff Assist - " + value + '<br/>';
					activity = activity + value + '<br/>';
                });
            });

			// Get diets
            url = main_url + "type=diet&mrn=" + mrn + "&numrec=4&sortorder=asc";
            var dataobj = '';
            var dietdesc = '';
			var diet = '';
            var x6 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {					
					value = ($(this).find("value").text());                    					
                    dietdesc = dietdesc + value;					
                });
            });
		
			// Get allergies
            url = main_url + "type=allergies&mrn=" + mrn + "&numrec=10&sortorder=asc";
            var dataobj = '';
            var allergies = '';			
			cnt = 0;
            var x7 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
					if (cnt != 0) 
					allergies = allergies + ', ';
                    allergies = allergies + ($(this).find("value").text());                    
					cnt = cnt + 1; 					
                });
				

            });

			// Get allergies
            url = main_url + "type=clinical&mrn=" + mrn + "&numrec=10&sortorder=asc";
            var dataobj = '';
            var clinicaldata = '';			
			cnt = 0;
            var x8 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
					code = ($(this).find("code").text());
					value = ($(this).find("value").text());					
					switch (code) {
						case 'ACES-1246':
							var numValue = Number(value);
							if(numValue<=18)
								clinicaldata = clinicaldata + '<div class="icon" id="braden"></div>';
								break;
						case 'ACES-7583':
							if(value!='NONE' && value != '')
								clinicaldata = clinicaldata + '<div class="icon" id="hearing"></div>';
								break;
						case 'ACES-7584':
							if(value!='NONE' && value != '')
								clinicaldata = clinicaldata + '<div class="icon" id="vision"></div>';
								break;
						case 'ACES-1375':
							if(value!='NONE' && value != '')
								clinicaldata = clinicaldata  + '<div class="icon" id="dentures"></div>';
								break;
						case 'ACES-7481':
							var numValue = Number(value);														
							if(numValue>=3)
								clinicaldata = clinicaldata  + '<div class="icon" id="fall"></div>';
								break;					
						};

                });
				
            });
			
			
			// Get pain
            url = main_url + "type=pain&mrn=" + mrn + "&numrec=10&sortorder=asc";
            var dataobj = '';
			var pain = '';
			var painscore = '';
			var painscoredate = '';

			cnt = 0;
            var x9 = Ion.fetcher.getXdXML(url, dataobj, true).done(function (data) {
                var xml = data;
                $(xml).find("item").each(function () {
					code = ($(this).find("code").text());
					value = ($(this).find("value").text());					
					switch (code) {
						case 'ACES-7617':					
							pain = value;
							break;
						case 'ACES-7615':					
							pain = value;
							break;
						case 'ACES-5695':										
							painscore = value;
							painscoredate = ($(this).find("startdate").text());			
							painscoredate = painscoredate.substr(0,16);
							break;
						case 'ACES-5690':					
							painscore = value;
							painscoredate = ($(this).find("startdate").text());			
							painscoredate = painscoredate.substr(0,16);
							break;		
						case 'ACES-1268':					
							painscore = value;
							painscoredate = ($(this).find("startdate").text());			
							painscoredate = painscoredate.substr(0,16);
							break;		
					};
                });
			});


			$.when(x1, x2, x3, x4, x5, x6, x7, x8, x9).done(function () {

				if(dietdesc.length >=1) {
					diet = diet + dietdesc+'<br/>';
				} else {		
					diet = diet + 'Please discuss your Dietary needs with your care team<br/>';
				}
				
				if(allergies.length >=1) {
					diet = diet + '<br/>Food Allergies: ' + allergies;
				} else {
					diet = diet + '<br/>Food Allergies: Unknown';
				}
				
				var pos = pain.indexOf("(");			
				if(pos >= 1) 
					pain = pain.substr(0,pos-1);
				pain = painscore + " - " + pain + " - " + painscoredate;
				
                self.$('#ecbddata-dischargedate').html(estimateddisdate);							
				self.$('#ecbddata-careteam').html(careteam);
				self.$('#ecbddata-goals').html(goals);
				self.$('#ecbddata-activity').html(activity);
				self.$('#ecbddata-diet').html(diet);
				self.$('#ecbddata-pain').html(pain);
				self.$('#icons').html(clinicaldata);				
            });

        });
    }

});