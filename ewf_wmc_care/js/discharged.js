/**
 * Discharged view module.
 */
var DischargedView = Ion.View.extend({

    template: 'discharged.html',
    css: 'discharged.css',
    id: 'discharged',

    render: function () {
        var context = this;
        this.addWidget('clock', new Ion.Widget.Clock({
            el: this.$('#date .header2 p')[0],
            format: 'dddd, mmm d - h:MM TT'
        }));

        var userData = Ion.fetcher.getUserData();
        var room = userData.home.roomNumber.toUpperCase();
        var basicinfo = "Room: " + room + '   |   Phone: ' + userData.home.phoneNumber + ""
        this.$('#basicinfo #roominfo').text(basicinfo);
        this.$('#basicinfo #name').html('&nbsp;');
        this.$('#room2').html("<p>Room: " + room + "</p>");

    },
});
