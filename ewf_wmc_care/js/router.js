/**
 * Per-Project router module. (note it does not have namespace 'Ion')
 */
// app level router

Ion.APP_FULL_NAME = 'Aceso UpCare';
Ion.APP_NAME = 'UpCare';
Ion.APP_VERSION = '1.0';
Ion.APP_TIMEZONE = 'PST8PDT';
Ion.APP_TIMEZONE_NAME = 'America/Los_Angeles';
    
(function ($) {
    var Router = Backbone.Router.extend({
        routes: {
            "":             "discharged",
            "careboard":    "careboard",    // #careboard
            "motherbaby":   "motherbaby",
            "discharged":   "discharged",
            "decline":   "decline"
        },
    
        careboard: function() {
            var view = new CareboardView({
                id: 'careboard',
                className: 'rotate90',
            });
            Ion.navigator.display(view);
        },

        motherbaby: function() {
            var view = new MotherbabyView({
                id: 'motherbaby',
                className: 'rotate90',
            });
            Ion.navigator.display(view);
        },
        
        decline: function() {
            var view = new DeclineView({ className: 'rotate90' });
            Ion.navigator.display(view);
        },
        
        discharged: function() {
            var view = new DischargedView({ className: 'rotate90' });
            Ion.navigator.display(view);
        }
        
    });
    
    Ion.router = new Router();
    
})($);

