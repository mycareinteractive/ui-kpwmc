/**
 * motherbaby view module.
 */
var MotherbabyView = Ion.View.extend({

    template: 'motherbaby.html',
    css: 'motherbaby.css',
    id: 'motherbaby',


    render: function () {
        this.addWidget('clock', new Ion.Widget.Clock({
            el: this.$('#date .header2 p')[0],
            format: 'dddd, mmm d - h:MM TT'
        }));
        var context = this;
        // schedule a timer every 60 seconds.
        $.doTimeout('motherbaby polling', 120000, function () {
            context._updateData();
            return true;
        });
        // delay 1 second and do an one-time update
        $.doTimeout('motherbaby polling once', 500, function () {
            context._updateData();
            return false;
        });

        return this;
    },

    uninit: function () {
        $.doTimeout('motherbaby polling');   // stop the timer
    },

    click: function ($obj) {
        Ion.log("** " + $obj.attr('id') + ' clicked **');
        return false;
    },

    _updateData: function () {

        //get data from middleware
        var userData = Ion.fetcher.getUserData();

        var cnt = '';
        // Get Patient MRN
        var url = '', dataobj = {};
        var preferredname = '';
        var mrn = Ion.fetcher.getMRN(true, false);

        var main_url = window.portalConfig.getclinical;


        // Get Patient Preferred Name
        url = main_url + "type=patient&mrn=" + mrn + "&numrec=3&sortorder=asc";

        var dataobj = '';
        var xml = Ion.fetcher.getXdXML(url, dataobj);

        $(xml).find("item").each(function () {
            preferredname = ($(this).find("value").text());
        });


        // Get Estimated Discharge Date
        url = main_url + "type=estimateddisdate&mrn=" + mrn + "&numrec=1&sortorder=asc";
        var estimateddisdate = '';
        var dataobj = '';
        var xml = Ion.fetcher.getXdXML(url, dataobj);
        $(xml).find("item").each(function () {
            var ed = ($(this).find("value").text());
            if (ed != '') {
                estimateddisdate = new Date(ed.substr(0, 4) + "/" + ed.substr(4, 2) + "/" + ed.substr(6, 2))
                estimateddisdate = estimateddisdate.format("mm-dd-yyyy");
            }
            Ion.log(estimateddisdate);
        });

        // Get Diets
        url = main_url + "type=diet&mrn=" + mrn + "&numrec=10&sortorder=dsc";
        var dataobj = '';
        var dietdesc = '<p>';
        var checktest = new Array;
        var xml = Ion.fetcher.getXdXML(url, dataobj);
        var code = '';
        $(xml).find("item").each(function () {
            desc = ($(this).find("codedescription").text());
            value = ($(this).find("value").text());
            status = ($(this).find("orderStatus").text());
            value = value.replace(/\;/g, '<br/>');
            value = value.replace(/\:/g, '<br/>');
            value = value.replace(/\,/g, '<br/>');
            if (status != 'Completed' && status != 'Exam Completed') {
                if (checktest[value] != value && value != ' ') {
                    checktest[value] = value;
                    dietdesc = dietdesc + value + '; ';
                }
            }
        });
        dietdesc = dietdesc + '</p>';


        var careteamcnt = 0;

        // Get Consults
        url = main_url + "type=consults&mrn=" + mrn + "&numrec=50&sortorder=asc";
        var dataobj = '';
        var enddate = '';
        var consults = '';
        cnt = 0;
        var xml = Ion.fetcher.getXdXML(url, dataobj);
        $(xml).find("item").each(function () {
            if (cnt <= 4) {
                desc = ($(this).find("codedescription").text());
                value = ($(this).find("value").text());
                enddate = ($(this).find("enddate").text());
                status = ($(this).find("orderStatus").text());
                var now = Ion.dateTime("yyyy-mm-dd HH:MM:ss");
                if (value != '    ') {
                    if (status != 'Completed' && status != 'Exam Completed') {
                        if (desc != '' && desc.length >= 1) {
                            consults = consults + '<p>' + desc + ':<span class="text-color-1">' + value + '</span></p>';
                        } else {
                            consults = consults + '<p>' + value + '</p>';
                        }
                        cnt = cnt + 1;
                        careteamcnt = careteamcnt + 1;
                    }
                }
            }
        });


        // Get Careteam Docs Data
        url = main_url + "type=careteam-docs&mrn=" + mrn + "&numrec=1&sortorder=asc";
        var dataobj = '';
        var enddate = '';
        var careteamDocs = '';
        cnt = 0;
        var xml = Ion.fetcher.getXdXML(url, dataobj);
        $(xml).find("item").each(function () {
            if (cnt <= 4) {
                desc = ($(this).find("codedescription").text());
                value = ($(this).find("value").text());
                enddate = ($(this).find("enddate").text());
                var now = Ion.dateTime("yyyy-mm-dd HH:MM:ss");
                var datecompare = 0;
                if (enddate.length >= 1)
                    datecompare = Ion.compareDates(enddate, now);
                Ion.log(datecompare + ' ' + value + ' ' + now + ' ' + enddate);
                if (datecompare >= 0) {
                    if (desc != '' && desc.length >= 1) {
                        careteamDocs = careteamDocs + '<p>' + desc + ':<span class="text-color-1">' + value + '</span></p>';
                    } else {
                        careteamDocs = careteamDocs + '<p>' + value + '</p>';
                    }
                    cnt = cnt + 1;
                    careteamcnt = careteamcnt + 1;
                }
            }
        });


        // Get Careteam Data
        url = main_url + "type=careteam&mrn=" + mrn + "&numrec=100&sortorder=asc";
        var dataobj = '';
        var enddate = '';
        var careteam = '';
        cnt = 0;
        var maxcareteam = 4 - careteamcnt;
        var xml = Ion.fetcher.getXdXML(url, dataobj);
        $(xml).find("item").each(function () {
            if (cnt <= maxcareteam) {
                desc = ($(this).find("codedescription").text());
                // decided not to show relationship
                //if (desc != 'Attending Physician')
                //	desc = ''
                value = ($(this).find("value").text());
                enddate = ($(this).find("enddate").text());
                var now = Ion.dateTime("yyyy-mm-dd HH:MM:ss");
                var datecompare = 0;
                if (enddate.length >= 1)
                    datecompare = Ion.compareDates(enddate, now);
                Ion.log(datecompare + ' ' + value + ' ' + now + ' ' + enddate);
                if (datecompare >= 0) {
                    if (desc != '' && desc.length >= 1) {
                        careteam = careteam + '<p>' + desc + ':<span class="text-color-1">' + value + '</span></p>';
                    } else {
                        careteam = careteam + '<p>' + value + '</p>';
                    }
                    cnt = cnt + 1;
                }
            }
        });


        // Get Activity
        url = main_url + "type=activity&mrn=" + mrn + "&numrec=6&sortorder=asc";
        var dataobj = '';
        var activity = '';
        var xml = Ion.fetcher.getXdXML(url, dataobj);
        var code = '';
        $(xml).find("item").each(function () {
            desc = ($(this).find("codedescription").text());
            value = ($(this).find("value").text());
            code = ($(this).find("code").text());
            status = ($(this).find("orderStatus").text());
            value = value.replace(/\;/g, '<br/>');
            value = value.replace(/\:/g, '<br/>');
            value = value.replace(/\,/g, '<br/>');

            if (status == 'Completed' || status == 'Exam Completed') {
                activity = activity + '<p>&#10004; ' + value + '</p>';
            } else {
                activity = activity + '<p>&nbsp;&nbsp;' + value + '</p>';
            }
            cnt = cnt + 1;
        });

        // Get mother
        url = main_url + "type=mother&mrn=" + mrn + "&numrec=10&sortorder=asc";
        var dataobj = '';
        var mother = '<br/>';
        var checktest = new Array;
        var xml = Ion.fetcher.getXdXML(url, dataobj);

        var cnt = 0;
        var spacer = "";
        $(xml).find("item").each(function () {
            spacer = "&nbsp;&nbsp;&nbsp;&nbsp;";
            cnt = cnt + 1;
            desc = ($(this).find("codedescription").text());
            value = ($(this).find("value").text());
            status = ($(this).find("orderStatus").text());

            if (status == 'Completed' || status == 'Exam Completed')
                spacer = '<span class="checkmark1">&#10004;</span>&nbsp;'
            if (checktest[value] != value) {
                checktest[value] = value;
                mother = mother + spacer + value + '<br/>';
            }
        });
        mother = '<p>' + mother + '</p>';

        // Get baby
        var getDependent_url = window.portalConfig.getDependent;

        var dataobj = '';
        url = getDependent_url + "patientMRN=" + mrn + '&';

        var babyXML = Ion.fetcher.getXdXML(url, dataobj);

        var cnt = 0;
        var baby = '<p><table>';
        var babyMRN = '';

        var babylist = new Array;

        var checktest = new Array;
        var babytest = new Array;

        $(babyXML).find("dependent").each(function () {
            babyMRN = $(this).find("dependentMRN").text();
            babylist[cnt] = babyMRN;

            url = main_url + "type=baby&mrn=" + babyMRN + "&numrec=10&sortorder=asc";
            var xml = Ion.fetcher.getXdXML(url, dataobj);
            var cnt2 = 0;
            var spacer = "&nbsp;&nbsp;&nbsp;&nbsp;";

            $(xml).find("item").each(function () {
                value = ($(this).find("value").text());
                status = ($(this).find("orderStatus").text());
                if (checktest[value] != value) {
                    checktest[value] = value;
                    babytest[value] = new Array(3);
                }
                babytest[value][cnt] = status;
                cnt2 = cnt2 + 1;
            });
            cnt = cnt + 1;
        });

        var babydata1 = '&nbsp;';
        var babydata2 = '&nbsp;';
        var babydata3 = '&nbsp;';

        for (var x in babytest) {
            var value = x;
            var data = '';
            for (var y in babytest[x]) {
                var status = babytest[x][y];

                if (status == 'Completed' || status == 'Exam Completed') {
                    data = '&#10004;'
                } else {
                    data = '&nbsp;';
                }
                if (y == 0)
                    babydata1 = '<span class ="checkmark1">' + data + '</span>';
                if (y == 1)
                    babydata2 = '<span class ="checkmark2">' + data + '</span>';
                if (y == 2)
                    babydata3 = '<span class ="checkmark3">' + data + '</span>';
            }
            console.log(y + '   3 ' + babydata3 + ' 2 ' + babydata2 + ' 1 ' + babydata1);
            baby = baby + '<tr><td>' + babydata3 + '<td>' + babydata2 + '<td>' + babydata1 + '<td>' + value + '</td></tr>';
        }
        baby = baby + '</table>';
        console.log(baby);


        // Get Support Person
        var mysupport = '';
        url = main_url + "type=supportperson&mrn=" + mrn + "&numrec=3&sortorder=asc";

        var dataobj = '';
        var xml = Ion.fetcher.getXdXML(url, dataobj);

        var mysupportperson = '';
        Ion.log('typof xml' + typeof xml.length);
        $(xml).find("item").each(function () {
            mysupportperson = unescape($(this).find("value").text());
        });
        Ion.log(mysupportperson);
        if (mysupportperson.length >= 1)
            mysupport = mysupport + '<p>' + mysupportperson + '</p>'

        if (preferredname) {
            this.$('#basicinfo #name').text('"' + preferredname + '"');
        } else {
            this.$('#basicinfo #name').text(userData.patient.userFullName);
        }


        this.$('#dischargedate').html('<div class="header2">Estimated Discharge:</div><p> ' + estimateddisdate + '</p>');

        careteam = careteamDocs + consults + careteam;
        var room = userData.home.roomNumber.toUpperCase();
        var basicinfo = "Room: " + room + '   |   Phone: ' + userData.home.phoneNumber + ""
        this.$('#basicinfo #roominfo').text(basicinfo);
        this.$('#careteam').html(careteam);
        this.$('#mother').html(mother);
        this.$('#baby').html(baby);
        this.$('#diet').html(dietdesc);
        this.$('#supportperson').html(mysupport);

    }


});