$(function(){
    var root = this;
    var iframe = document.getElementById('upcare-frame');
    iframe.onload = function() {
        var iframeWindow = this.contentWindow;
        iframeWindow.focus();
        $(this).toggleClass('fullscreen', iframeWindow.$('body').hasClass('fullscreen'));
    };
});